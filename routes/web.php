<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Static CMS Pages Routes
Route::get('/services','StaticPageController@index')->name('services');


Route::get('/',function () { return view('home');});
Route::get('about','AboutController@about')->name('about');
Route::get('about/about-management','AboutController@about_management')->name('about_management');
Route::get('about/about-directors','AboutController@about_directors')->name('about_directors');
Route::get('solutions/digital-banking','SolutionsController@digital_banking')->name('digital_banking');
Route::get('solutions/digital-wallet','SolutionsController@solutions')->name('solutions');
Route::get('solutions/finzo-pay','SolutionsController@finzo_pay')->name('finzo_pay');
//Route::get('services','ServicesController@services')->name('services');
Route::get('contact','ContactController@contact')->name('contact');
Route::post('/insert', 'ContactUSController@insert');

Route::get('/changePassword','Auth\ResetPasswordController@showChangePasswordForm');

Route::post('/changePasswordToken','Auth\ResetPasswordController@resetPasswordWithToken');

Route::post('/changePassword','Auth\ResetPasswordController@changePassword')->name('changePassword');

Route::get('/passwordExpiration','Auth\PasswordExpirationController@showPasswordExpirationForm');
Route::post('/passwordExpiration','Auth\PasswordExpirationController@postPasswordExpiration')->name('passwordExpiration');
Route::get('/cms','Admin\Cms\CmsController@index');

// ADMIN ROUTES
Auth::routes();
Route::prefix('admin')->middleware('auth','checkPermissions')->group(function () {

    Route::get('home', 'Admin\HomeController@index')->name('dashboard');

    // MEDIA MANAGER SECURITY ROUTES
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');

    // ROLES ROUTES
    Route::resource('roles', 'Admin\Acl\RolesController');

    Route::resource('offers', 'Admin\Offers\OffersController');

    Route::resource('pages', 'Admin\Cms\PageController');

    // USERS ROUTES
    
    Route::any('permission/user-permission/{id}','Admin\User\UsersController@assignPermission');
    Route::any('users/save','Admin\User\UsersController@storePermission');
    Route::resource('users', 'Admin\User\UsersController');
    // PERMISSION ROUTES
    Route::any('permission/assign-permission','Admin\Acl\PermissionController@assignPermission')->name('view.roles.permission');
    Route::any('permission/save','Admin\Acl\PermissionController@storePermissionRole')->name('assign.roles.permission');
    Route::resource('permissions', 'Admin\Acl\PermissionController');

    // LOGS ROUTES
    Route::resource('logs', 'Admin\Logs\LogsController');

});


