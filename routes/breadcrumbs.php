<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > About
Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push('About', route('about'));
});

// Home > About > About Management
Breadcrumbs::for('about_management', function ($trail) {
    $trail->parent('about');
    $trail->push('About Management', route('about_management'));
});

// Home > About > About Directors
Breadcrumbs::for('about_directors', function ($trail) {
    $trail->parent('about');
    $trail->push('About Directors', route('about_directors'));
});

// Home > Solutions 
Breadcrumbs::for('solutions', function ($trail) {
    $trail->parent('home');
    $trail->push('Solutions', route('solutions'));
});

// Home > Solutions > Digital Wallet
Breadcrumbs::for('digital_wallet', function ($trail) {
    $trail->parent('solutions');
    $trail->push('Digital Wallet', route('solutions'));
});

// Home > Solutions > Finzo Pay
Breadcrumbs::for('finzo_pay', function ($trail) {
    $trail->parent('solutions');
    $trail->push('Finzo Pay', route('finzo_pay'));
});

// Home > Solutions > Digital Banking
Breadcrumbs::for('digital_banking', function ($trail) {
    $trail->parent('solutions');
    $trail->push('Digital Banking', route('digital_banking'));
});

// Home > Services 
Breadcrumbs::for('services', function ($trail) {
    $trail->parent('home');
    $trail->push('Services', route('services'));
});

// Home > Contact 
Breadcrumbs::for('contact', function ($trail) {
    $trail->parent('home');
    $trail->push('Contact', route('contact'));
});

?>