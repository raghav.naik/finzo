@extends('layouts.master')
@section('pageTitle', 'About Us')
@section('content')
<!-- Main Section -->
      <div class="page-wrapper opr-current">
          @include('includes.aboutBottomNav')
        <div class="cover__slider">
          <div class="opr-page">
            <nav class="breadcrumbs animatable">
              {{ Breadcrumbs::render('about') }}
            </nav>
            <section class="head-styled-section vision">
              <div class="head-styled animatable">
                <div class="container">
                  <h4>About Us</h4>
                </div>
              </div>
              <div class="content-styled">
                <div class="container">
                  <div class="parallax-layer" id="parallaxSecond">
                    <img data-depth="0.80" src="{{ URL::asset('images/global-map.png') }} " alt="">
                  </div>
                  <div class="row">
                    <div class="col-12 col-md-7 animatable">
                      <h4>Vision</h4>
                      <div class="large-text">
                        To be the partner of choice for fintech solutions globally!
                      </div>
                    </div>
                    <div class="col-12 col-md-5 revealer">
                      <div class="revealer__image animatable">
                        <img src="{{ URL::asset('images/about-vision.jpg') }}" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="content-styled-section mission">
              <div class="container">
                <div class="row revealer">
                  <div class="col-12 col-md-6 content-styled-section-fir">
                    <div class="revealer__image animatable">
                      <img src="{{ URL::asset('images/about-mission.jpg') }}" alt="">
                    </div>
                  </div>
                  <div class="col-12 col-md-6 content-styled-section-sec">
                    <div class="revealer__text animatable">
                      <h4 data-depth="0" >Mission</h4>
                      <div class="medium-text">
                        To redefine payments & banking experiences by delivering cutting edge technology solutions.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <footer>
        <div class="row opr-common justify-content-between">
          <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#">
              <span>Fintech Innovation in a box</span>
              <i class="circle__move"></i>
            </a>
          </div>
          <div class="buttons animatable">
            <div class="devider_button pjax-class">
              <a class="button button_with-icon" href="{{ URL::asset('/home/about/about-management') }}">
                <span>Management</span>
                <i class="icon-play"></i>
              </a>
            </div>
            <div class="text-right bottom__footer">
              <buttton class="button button_with-icon button_normal top-btn">
                <span>Top</span>
                <i class="circle__move"></i>
              </buttton>
              <p>&copy; 2019 Finzo</p>
            </div>
          </div>
        </div>
      </footer>
@stop
