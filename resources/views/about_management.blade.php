@extends('layouts.master')
@section('pageTitle', 'Management and Team')
@section('content')
<!-- Main Section -->
      <div class="page-wrapper opr-current">
        @include('includes.aboutBottomNav')
        <div class="cover__slider">
          <div class="opr-page">
            <nav class="breadcrumbs animatable">
           {{ Breadcrumbs::render('about_management') }}
            </nav>
             <section class="team-grid devider-section">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-md-3 side-bar-styled"></div>
                  <div class="col-12 col-md-9">
                    <div class="team-grid-header animatable">
                      <h4>Management</h4>
                      <h2>Banking, Leadership and Management Expertise</h2>
                      
                    </div>
                    <div class="team-grid-content">
                      <div class="row">
                        <div class="col-12 col-md-6">
                          <div class="team__member revealer">
                            <div class="revealer__image animatable">
                              <a class="grid-overlay" href="#" data-toggle="modal" data-target="#member1">
                                <i class="icon-view"></i>
                              </a>
                              <img src="{{ URL::asset('images/management/shiraz-ali.jpg') }}" alt="">
                            </div>
                          </div>
                          <a href="#" class="member-tagline" data-toggle="modal" data-target="#member1">Shiraz Ali</a>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="team__member revealer">
                            <div class="revealer__image animatable">
                              <a class="grid-overlay" href="#" data-toggle="modal" data-target="#member2">
                                <i class="icon-view"></i>
                              </a>
                              <img src="{{ URL::asset('images/management/vivek_jain.jpg') }}" alt="">
                            </div>
                          </div>
                          <a href="#" class="member-tagline" data-toggle="modal" data-target="#member2">Vivek Jain</a>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="team__member revealer">
                            <div class="revealer__image animatable">
                              <a class="grid-overlay" href="#" data-toggle="modal" data-target="#member3">
                                <i class="icon-view"></i>
                              </a>
                              <img src="{{ URL::asset('images/management/rajneesh_chopra.jpg') }}" alt="">
                            </div>
                          </div>
                          <a href="#" class="member-tagline" data-toggle="modal" data-target="#member3">Rajneesh Chopra</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <footer class="devider-section">
        <div class="row opr-common justify-content-between">
          <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#">
              <span>Fintech Innovation in a Box</span>
              <i class="circle__move"></i>
            </a>
          </div>
          <div class="buttons animatable">
            <div class="devider_button pjax-class">
              <a class="button button_with-icon" href="{{ URL::asset('/home/about/about-directors') }}">
                <span>Board of Directors</span>
                <i class="icon-play"></i>
              </a>
            </div>
            <div class="text-right bottom__footer">
              <buttton class="button button_with-icon button_normal top-btn">
                <span>Top</span>
                <i class="circle__move"></i>
              </buttton>
              <p>Finzo Co. B.S.C. (c) All Rights Reserved</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div class="modal fade fz-modal" id="member1" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="modal-close" data-dismiss="modal">
            <i class="icon-close"></i>
          </button>
          <div class="row revealer">
            <div class="col-12 col-md-4 p-0">
              <div class="revealer__image animatable">
                <img src="{{ URL::asset('images/management/shiraz-ali.jpg') }}" alt="">
              </div>
            </div>
            <div class="col-12 col-md-8 p-0">
              <div class="revealer__text animatable">
                <div href="#" class="member-tagline">
                  <span>Shiraz Ali</span>
                  <span>C E O</span>
                </div>
                <div class="modal-text">
                  <p>Shiraz has over 16 years of professional experience, working with central bank, commercial bank, consulting and payments companies across Pakistan and Middle East. Prior to joining AFS in July 2008, Shiraz was associated with Al-Rajhi Islamic Bank (KSA) at a senior position in audit function. He has also worked in State Bank of Pakistan (Central Bank) where he helped the bank in transforming a traditional internal audit function into a robust assurance and consulting service designed for value addition.</p>
                  <p>Shiraz has held various roles at AFS including Head of Internal Audit, Head of Strategy Management Office and is currently working as Director – Frictionless Payments Business. His expertise include product development and management (especially mobile payments and digital wallets), ecosystem development, strategy and P&L management.</p>
                  <p>He has an MBA from the Institute of Business Administration (IBA), Karachi and B.E. (CS) from NED University of Engineering & Technology, Pakistan. He also holds CIA, CISA and CRMA certifications and is an associate member of ACFE and ACCA.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade fz-modal" id="member2" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="modal-close" data-dismiss="modal">
            <i class="icon-close"></i>
          </button>
          <div class="row revealer">
            <div class="col-12 col-md-4 p-0">
              <div class="revealer__image animatable">
                <img src="{{ URL::asset('images/management/vivek_jain.jpg') }}" alt="">
              </div>
            </div>
            <div class="col-12 col-md-8 p-0">
              <div class="revealer__text animatable">
                <div href="#" class="member-tagline">
                  <span>Vivek Jain</span>
                  <span>Head of Business Development</span>
                </div>
                <div class="modal-text">
                  <p>With 27+ years of industry experience in the banking segment Vivek has been focused on driving business relationships & strategic alliances.  He has worked across retail assets, payments, product and has significant experience in audit & compliance, operational & credit risk assessment.  His responsibilities have spanned across creating revenue funnels for credit cards, and formulating strategies for cross sells alongside insurance segments.</p>
                  <p>Vivek joined AFS in 2012 as the Head of Client Relationship, responsible for consulting and managing 50+ large client banks across Middle East & Levant to drive cards & retail assets penetration. He introduced various other tools like Frictionless Payments, Digital Wallets, Mobile Payments, Affluent Cards, Instant Issuance of Cards, Loyalty Program, Data Analytics, Remittance, Fraud Monitoring & Management and others. He took over the role of Head of Business Development for Fintech in 2017.</p>
                  <p>Prior to joining AFS, Vivek had worked for Standard Chartered Bank in 3 different geographies in various leadership roles in Consumer Banking.</p>
                  <p>Vivek studied English Literature at the University of Delhi, India</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade fz-modal" id="member3" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="modal-close" data-dismiss="modal">
            <i class="icon-close"></i>
          </button>
          <div class="row revealer">
            <div class="col-12 col-md-4 p-0">
              <div class="revealer__image animatable">
                <img src="{{ URL::asset('images/management/rajneesh_chopra.jpg') }}" alt="">
              </div>
            </div>
            <div class="col-12 col-md-8 p-0">
              <div class="revealer__text animatable">
                <div href="#" class="member-tagline">
                  <span>Rajneesh Chopra</span>
                  <span>Marketing Director </span>
                </div>
                <div class="modal-text">
                  <p>Rajneesh has over 27 years immersive business side, marketing & online experiences in both B2C and B2B companies including mobile wallets, digital payments, ecommerce & online content with domestic and International companies.</p>
                  <p>Prior to his current role, Rajneesh had setup and scaled digital payments business to deliver over 150,000 real time transactions daily and managed to acquire & monetize over 14 million users.  He has been a serial entrepreneur and build digital assets in areas of data analytics, predictive algorithms and location based solutions.</p>
                  <p>He has worked with Microsoft as Director for Window Live platform and Head of Marketing for the digital arm of CNN and CNBC in India where he scaled up MoneyControl.com - Asia’s Largest Financial News Website. </p>
                  <p>He is an MBA from The University of the District of Columbia, USA and a certified digital analyst and email marketing professional.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @stop

