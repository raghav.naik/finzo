@extends('layouts.app')
@section('content')
<div class="card card-shadow mb-4">
    <div class="card-header">
        <div class="card-title">
            Login
        </div>
    </div>
    <div class="card-body">
      <form method="POST" action="{{ route('login') }}" novalidate>
          @csrf
          <div class="form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
            <label>Email address Or UserName</label>
              <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' has-input' : '' }}" name="email" value="{{ old('email') }}" autofocus placeholder="Email Or UserName">
                @if ($errors->has('email'))
                <small class="form-text">
                  <strong>{{ $errors->first('email') }}</strong>
                </small>
                @endif
          </div>
          <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
            <label>Password</label>
              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' has-input' : '' }}" name="password" placeholder="Password">
                @if ($errors->has('password'))
                <small class="form-text">
                  <strong>{{ $errors->first('password') }}</strong>
                </small>
                @endif
          </div>
          {{-- <div class="checkbox">
              <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <span class="custom-control-indicator"></span> <span class="custom-control-description">Remember Me</span>
              </label>
            <label>
              </label>
              <label class="pull-right">
                <a href="{{ route('password.request') }}">
                    Forgot Your Password
                </a>
              </label>
          </div> --}}
          <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">
            Login
          </button>
      </form>
    </div>
</div>
@endsection



