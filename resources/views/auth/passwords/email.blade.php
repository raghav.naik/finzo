@extends('layouts.app')
@section('content')
<div class="card card-shadow mb-4">
    <div class="card-header">
        <div class="card-title">
            Reset Password
        </div>
    </div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}" novalidate>
            @csrf
            <div class="form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
                <label>E-Mail Address</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' has-input' : '' }}" name="email">
                    @if ($errors->has('email'))
                    <small class="form-text">
                        <strong>{{ $errors->first('email') }}</strong>
                    </small>
                    @endif
            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">
                Send Password Reset Link
            </button>
        </form>
    </div>
</div>
@endsection
