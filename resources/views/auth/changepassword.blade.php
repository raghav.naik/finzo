@extends('layouts.app')
@section('content')
<div class="card card-shadow mb-4">
    <div class="card-header">
        <div class="card-title">
            Login
        </div>
    </div>
    <div class="card-body">
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <form method="POST" method="POST" action="{{ route('changePassword') }}">
            @csrf
            <div class="form-group {{ $errors->has('current-password') ? 'has-danger' : '' }}">
            <label>Email address Or UserName</label>
            <input id="current-password" type="password" class="form-control" name="current-password">
                @if ($errors->has('current-password'))
                <small class="form-text">
                    <strong>{{ $errors->first('current-password') }}</strong>
                </small>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
            <label>New Password</label>
            <input id="new-password" type="password" class="form-control" name="new-password">
                @if ($errors->has('password'))
                <small class="form-text">
                    <strong>{{ $errors->first('password') }}</strong>
                </small>
                @endif
            </div>
            <div class="form-group {{ $errors->has('new-password_confirmation') ? 'has-danger' : '' }}">
            <label>Confirm New Password</label>
            <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation">
                @if ($errors->has('new-password_confirmation'))
                <small class="form-text">
                    <strong>{{ $errors->first('new-password_confirmation') }}</strong>
                </small>
                @endif
            </div>
            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">
            Change Password
            </button>
        </form>
    </div>
</div>
@endsection
