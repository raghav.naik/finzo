@extends('layouts.master')
@section('pageTitle', 'Directors Team')
@section('content')

 <!-- Main Section -->
      <div class="page-wrapper opr-current">
         @include('includes.aboutBottomNav')
        <div class="cover__slider">
          <div class="opr-page">
            <nav class="breadcrumbs animatable">
              {{ Breadcrumbs::render('about_directors') }}
            </nav>
             <section class="team-grid devider-section">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-md-3 side-bar-styled"></div>
                  <div class="col-12 col-md-9">
                    <div class="team-grid-header animatable">
                      <h4>Board of Directors</h4>
                      <h2>Banking, Leadership and Management Expertise</h2>
                    
                    </div>
                    <div class="team-grid-content">
                      <div class="row">
                        <div class="col-12 col-md-6">
                          <div class="team__member revealer">
                            <div class="revealer__image animatable">
                              <a class="grid-overlay" href="#" data-toggle="modal" data-target="#member1">
                                <i class="icon-view"></i>
                              </a>
                              <img src="{{ URL::asset('images/management/seal_al_waary.jpg') }}" alt="">
                            </div>
                          </div>
                          <a href="#" class="member-tagline" data-toggle="modal" data-target="#member1">Mr. Sael Al Waary</a>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="team__member revealer">
                            <div class="revealer__image animatable">
                              <a class="grid-overlay" href="#" data-toggle="modal" data-target="#member2">
                                <i class="icon-view"></i>
                              </a>
                              <img src="{{ URL::asset('images/management/maher_kaddoura.jpg') }}" alt="">
                            </div>
                          </div>
                          <a href="#" class="member-tagline" data-toggle="modal" data-target="#member2">Mr. Maher Kaddoura</a>
                        </div>
                        <div class="col-12 col-md-6">
                          <div class="team__member revealer">
                            <div class="revealer__image animatable">
                              <a class="grid-overlay" href="#" data-toggle="modal" data-target="#member3">
                                <i class="icon-view"></i>
                              </a>
                              <img src="{{ URL::asset('images/management/chandrashekhar.jpg') }}" alt="">
                            </div>
                          </div>
                          <a href="#" class="member-tagline" data-toggle="modal" data-target="#member3">B Chandrasekhar</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <footer class="devider-section">
        <div class="row opr-common justify-content-between">
          <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#">
              <span>Fintech Innovation in a Box</span>
              <i class="circle__move"></i>
            </a>
          </div>
          <div class="buttons animatable">
            <!-- <div class="devider_button">
              <a class="button button_with-icon" href="#">
                <span>Management</span>
                <i class="icon-play"></i>
              </a>
            </div> -->
            <div class="text-right bottom__footer">
              <buttton class="button button_with-icon button_normal top-btn">
                <span>Top</span>
                <i class="circle__move"></i>
              </buttton>
              <p>&copy; Finzo Co. B.S.C. (c) All Rights Reserved</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div class="modal fade fz-modal" id="member1" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="modal-close" data-dismiss="modal">
            <i class="icon-close"></i>
          </button>
          <div class="row revealer">
            <div class="col-12 col-md-4 p-0">
              <div class="revealer__image animatable">
                <img src="{{ URL::asset('images/management/seal_al_waary.jpg') }}" alt="">
              </div>
            </div>
            <div class="col-12 col-md-8 p-0">
              <div class="revealer__text animatable">
                <div href="#" class="member-tagline">
                  <span>Mr. Sael Al Waary</span>
                  <span>Chairman</span>
                </div>
                <div class="modal-text">
                  <p>B.Sc. (Hons) degree in Computer Sciences, University of Reading, United Kingdom.</p>
                  <p>Mr. Al Waary is currently Deputy Group Chief Executive Officer of the Bank ABC Group, which he joined in 1981. He brings with him more than 33 years of banking, leadership and management experience garnered from the many senior positions he has held in both London and Bahrain.  Mr. Al Waary is also the Chairman of Bank ABC Jordan and Chairman of Arab Financial Services Company B.S.C.(c) and has previously served on the Boards of Banco ABC Brasil and Arab Banking Corporation Egypt S.A.E.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade fz-modal" id="member2" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="modal-close" data-dismiss="modal">
            <i class="icon-close"></i>
          </button>
          <div class="row revealer">
            <div class="col-12 col-md-4 p-0">
              <div class="revealer__image animatable">
                <img src="{{ URL::asset('images/management/maher_kaddoura.jpg') }}" alt="">
              </div>
            </div>
            <div class="col-12 col-md-8 p-0">
              <div class="revealer__text animatable">
                <div href="#" class="member-tagline">
                  <span>Mr. Maher Kaddoura</span>
                  <span>Director</span>
                </div>
                <div class="modal-text">
                  <p>Maher Kaddoura is a management consultant, a serial entrepreneur, angel investor, social change maker, public speaker and adventurer:</p>
                  <ul>
                    <li><strong>Entrepreneur & Angel Investor:</strong> Upon retirement as Accenture-Middle East managing partner, he established a number of professional services firms that became regional leaders in their relative sectors (strategy consulting, system integration and training). He invests and serves on the board of a number of companies in the following sectors: knowledge economy, food & beverage, electro-mechanical, retail, micro finance and financial services.</li>
                    <li><strong>Social Change Maker:</strong> Maher established Al-Jude NGO in 1999 and launched a number of social programs focused on road safety, youth leadership, innovation and entrepreneurship. Maher is a visionary who believes in achieving more with less for many using his own theory named "The Falafel Theory".</li>
                    <li><strong>Board Member:</strong> Maher is also active in a number NGO boards including research on aging, youth development, entrepreneurship, micro-ventures, health services standards.</li>
                    <li><strong>Thought Contributor:</strong> Maher is a public speaker and storyteller. He shares his experience and thinking on social media using Snackable format of videos, digital books, and visual posts. King Abdullah II, Sheikh Mohammad Ben Rashid of Dubai, United Nations and Ernest & Young recognized him. </li>
                    <li><strong>Adventurer & World Traveler:</strong> Maher is a street photographer, world traveler who travelled to more than 60 countries and 100 cities. He is an adventurer who trekked to Everest Base Camp, the Arctic, Alaska, the Jungles of Africa and the mountains of South America. </li>
                  </ul>
                  <p><strong>Social Change Maker:</strong> He established a number of high impact social programs including: </p>
                  <ul>
                    <li><strong>Hikmat Road Safety:</strong> is a program to address Road Safety and reduce the percentage of road death and injuries in Jordan.  </li>
                    <li><strong>Playground:</strong> Built 1200 football playgrounds in public schools with a national neighborhood football championship involving 500 neighborhoods.</li>
                    <li><strong>MeshMosta7eel:</strong> It is a social TV show where every week participants present their innovative ideas to solve a specific national challenge and win a prize. The purpose of the program is to fight complacency and plant the seeds of innovation in our culture. </li>
                    <li><strong>Hikmat Leaders:</strong> a 4-months leadership program for university students in their last year to sharpen their skills through carefully designed leadership simulation scenarios and experiences, so participants gain experience and knowledge through acting as leaders not lectured.</li>
                    <li><strong>Newthink Discover:</strong> is a 2 day new age learning event held to help organizations and professionals increase their effectiveness through attending training sessions delivered by experts in the field, and by developing their skills for a more productive workforce.</li>
                    <li><strong>Sherkitna:</strong> program to plant the seeds of entrepreneurship among Jordanian students which now included 650 school companies.</li>
                    <li><strong>Meqdam:</strong> a program to plant the seeds of leadership in today’s youth.</li>
                    <li><strong>Newthink:</strong> programs designed to inspire youth to unleash their potential. It includes a theater, summer outdoor festival, and a learning institute. This initiative was awarded Arab Social Media influencers Award in 2015. </li>
                    <li><strong>“Fikrezyon” On-line TV:</strong> the first Arabic online TV focused on spreading hope and curiosity.</li>
                    <li><strong>Sonbola:</li> is a social innovation incubator focused on young social entrepreneurs.</li>
                  </ul>
                  <p>
                    <address>
                      He can be followed on: <br/>
                      Twitter & Instagram: @maherkaddoura <br/>
                      Facebook :www.facebook.com/maher.kaddoura.3. <br/>
                      Website: maherkaddoura.org <br/>
                      Linkedin: Maher Kaddoura <br/>
                    </address>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade fz-modal" id="member3" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <button type="button" class="modal-close" data-dismiss="modal">
            <i class="icon-close"></i>
          </button>
          <div class="row revealer">
            <div class="col-12 col-md-4 p-0">
              <div class="revealer__image animatable">
                <img src="{{ URL::asset('images/management/chandrashekhar.jpg')}}" alt="">
              </div>
            </div>
            <div class="col-12 col-md-8 p-0">
              <div class="revealer__text animatable">
                <div href="#" class="member-tagline">
                  <span>B Chandrasekhar</span>
                  <span>Director</span>
                </div>
                <div class="modal-text">
                  <p>Chandrasekhar has over 30 years of experience in retail banking and payments industry across multiple geographies and roles. He has worked for over 23 years with Standard Chartered Bank across India, Bahrain, Lebanon and UAE. He joined Arab Banking Corporation in August 2010 as Group Head of Products and Marketing and he is the CEO of AFS since March 2011.</p>
                  <p>During his work experience with Standard Chartered Bank, he held the positions of Regional Head of Credit Cards and Personal Loans for Middle East, South Asia and Africa from 2003 to 2008 and was the Regional Head of Consumer Banking for Northern Gulf, Lebanon and Jordan from 2008-2010. Chandrasekhar was also a Board Director on the BENEFIT company in Bahrain from 2008-2010.</p>
                  <p>Chandrasekhar holds an MBA from the Indian Institute of Management Calcutta and a Bachelor of Engineering Degree from Delhi College of Engineering.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @stop
