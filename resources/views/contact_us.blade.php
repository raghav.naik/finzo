@extends('layouts.master')
@section('pageTitle', 'Contact')
@section('content')
@section('contents')
        <div class="header__menu_inner">
          <a class="header__logo" href="{{url('/')}}">
         <img src="images/logo.png" alt></a>
          </div>
  @endsection
    <!-- Main Section -->
    <div class="page-wrapper opr-current">
      <section class="cover">
        <div class="cover__bg">
          <img src="images/contact/contact_bg.jpg" alt="">
        </div>
        <div class="cover__overlay main__cover animatable">
          <h1>Contact</h1>
        </div>
        <div class="bottom__nav">
          <div id="dot-target" class="cover-pagination opr-nav">
            <a href="#" class="contact-bullet1 cover-pagination-bullet cover-pagination-bullet-active">
            <span>Get in Touch</span>
            <span></span>
            </a>
            <a href="#contact-sec" class="cover-pagination-bullet contact-bullet">
            <span>Contact US</span>
            <span></span>
            </a>
          </div>
        </div>
      </section>
      <nav class="breadcrumbs animatable">
        <a href="{{ URL::asset('/') }}" title="Home">Home</a>
        <a href="{{ URL::asset('/contactUS') }}" title="About">Contact</a>
      </nav>
      <div class="head-styled animatable p-0">
        <div class="container">
          <div class="row">
            <div class="col-12 col-md-5">
              <h4 class="custom-heading">Get in Touch</h4>
            </div>
            <div class="col-12 col-md-7 pl-custom mb-4-sm">
              <h2>We’d love to hear from you, get in touch </h2>
            </div>
          </div>
        </div>
      </div>
      <section class="head-styled-section vision">
        <div class="container contact-section">
          <div class="parallax-layer" id="parallaxSecond">
            <img data-depth="0.80" src="images/global-map.png" alt="">
          </div>
          <div class="row">
            <div class="col-md-6 d-flex">
              <img src="images/contact/contact_main.jpg" class="contact-img-left">
            </div>
            <div class="col-md-6 d-flex vertical-align-middle p-70">
              <form class="custom-form">
                <fieldset>
                  <div class="row form-elements animatable">
                    <div class="col-12">
                      <div class="input-wrapper">
                        <input placeholder="Name" class="cf-input" type="text" name="name" />
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="input-wrapper">
                        <input placeholder="Email" class="cf-input" type="text" name="email" />
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="input-wrapper">
                        <input placeholder="Phone" class="cf-input" type="text" name="phone" />
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="input-wrapper cf-select-box">
                        <select class="cf-input" style="display: none;">
                          <option>Digital Wallet</option>
                          <option>Finzo Pay</option>
                          <option>Digital Banking</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="input-wrapper">
                        <textarea placeholder="Message Here" class="cf-input" name="textarea" cols="5" rows="5"></textarea>
                      </div>
                    </div>
                    <div class="col-12" id="contact-sec">
                      <button class="btn-submit-custom pl-3 pr-3 pt-2 pb-2">Submit</button>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
    </div>
    </section>
    <section class="address-map" >
      <div class="container mt-18">
        <div class="row">
          <div class="col-md-12 col-12 align-items-end contact-details-container">
            <div class="contact-details">
              <h4 class="pb-3 text-capitalize" style="margin-bottom: 18px !important;">Contact Us</h4>
              <h4 class="primary-heading text-capitalize">Phone: <span>+973 17 290333</span></h4>
              <h4 class="primary-heading text-capitalize">Fax: <span>+973 17 290050</span></h4>
              <h4 class="primary-heading text-capitalize">Email Address: <span class="text-lowercase">marketing@afs.com.bh</span></h4>
              <h4 class="text-capitalize pt-4 pb-3" style="margin-bottom: 18px !important;">For Support, Please Contact</h4>
              <h4 class="primary-heading text-capitalize">Address:</h4>
              <h4>P.O.Box 2152, Manama, Kingdom of Bahrain</h4>
            </div>
            <div class="custom-map">
               <img src="images/contact/contact_map.jpg">
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer>
      <div class="row opr-common justify-content-between">
        <div class="buttons finetech-btn animatable">
          <a class="button button_with-icon button_normal" href="#">
          <span>Fintech Innovation in a box</span>
          <i class="circle__move"></i>
          </a>
        </div>
        <div class="buttons animatable">
          <div class="devider_button">
            <a class="button button_with-icon p-4">
            <span></span>
            </a>
          </div>
          <div class="text-right bottom__footer">
            <buttton class="button button_with-icon button_normal top-btn">
              <span>Top</span>
              <i class="circle__move"></i>
            </buttton>
            <p>&copy; 2019 Finzo</p>
          </div>
        </div>
      </div>
    </footer>
@stop