<div class="header__menu_inner">
  <a class="header__logo" href="{{url('/')}}">
  <img src="{{URL::asset('images/logo.png') }}" alt></a>
  <nav class="sticky--nav">
    <ul>
      <li>
        <a href="{{url('/home/solutions/digital-wallet')}}">Digital Wallet</a>
      </li>
      <li>
        <a href="{{url('/home/solutions/finzo-pay')}}">FinzoPay</a>
      </li>
      <li>
        <a href="{{url('/home/solutions/digital-banking')}}">Digital Banking</a>
      </li>
     </ul>
  </nav>
</div>
 