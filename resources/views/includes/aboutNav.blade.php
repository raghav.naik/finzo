<div class="header__menu_inner">
  <a class="header__logo" href="{{url('/home')}}">
  <img src="{{URL::asset('images/logo.png') }}" alt></a>
  <nav class="sticky--nav">
    <ul>
      <li >
        <a href="{{URL::asset('/home/about')}}" class="{{$class}}">About</a>
      </li>
      <li>
        <a href="{{URL::asset('/home/about/about-management')}}" class="{{$class}}">Management</a>
      </li>
      <li>
        <a href="{{URL::asset('/home/about/about-directors')}}" class="{{$class}}">Board of Directors</a>
      </li>
    </ul>
  </nav>
</div>