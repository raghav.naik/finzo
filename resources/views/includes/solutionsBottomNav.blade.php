 <section class="cover">
          <div class="cover__bg">
            <img src="{{ URL::asset('images/Solutions-banner.jpg') }}" alt="">
          </div>
          <div class="cover__overlay main__cover animatable">
            <h1>Solutions</h1>
          </div>
          <div class="bottom__nav">
            <div id="dot-target" class="cover-pagination opr-nav $class">
              <a href="{{ URL::asset('/home/solutions/digital-wallet') }}" class="cover-pagination-bullet">
                <span>Digital Wallet</span>
                <span></span>
              </a>
              <a href="{{ URL::asset('/home/solutions/finzo-pay') }}" class="cover-pagination-bullet $class">
                <span>Finzo Pay</span>
                <span></span>
              </a>
              <a href="{{ URL::asset('/home/solutions/digital-banking') }}" class="cover-pagination-bullet cover-pagination-bullet $class">
                <span>Digital Banking</span>
                <span></span>
              </a>
            </div>
          </div>
        </section>
