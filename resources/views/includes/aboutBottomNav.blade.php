    <section class="cover">
          <div class="cover__bg">
            <img src="{{ URL::asset('images/about-banner.jpg') }}" alt="">
          </div>
          <div class="cover__overlay main__cover animatable">
            <h1>About Us</h1>
          </div>
          <div class="bottom__nav">
            <div id="dot-target" class="cover-pagination opr-nav pjax-class">
              <a href="{{ URL::asset('/home/about') }}" class="cover-pagination-bullet cover-pagination-bullet $class">
                <span>About</span>
                <span></span>
              </a>
              <a href="{{ URL::asset('/home/about/about-management') }}" class="cover-pagination-bullet $class">
                <span>Management</span>
                <span></span>
              </a>
              <a href="{{ URL::asset('/home/about/about-directors') }}" class="cover-pagination-bullet $class">
                <span>Board of directors</span>
                <span></span>
              </a>
            </div>
          </div>
        </section>