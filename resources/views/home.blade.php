@extends('layouts.home_master')
@section('content')
    <!-- Main Section -->
    <div class="page-wrapper">
      <div class="bottom__nav">
        <div id="dot-target" class="cover-pagination opr-nav">
          <a href="#" class="cover-pagination-bullet cover-pagination-bullet-active" data-target="1">
            <span>intro</span>
            <span></span>
          </a>
          <a href="#" class="cover-pagination-bullet" data-target="2">
            <span>digital wallet</span>
            <span></span>
          </a>
          <a href="#" class="cover-pagination-bullet" data-target="3">
            <span>digital banking</span>
            <span></span>
          </a>
          <a href="#" class="cover-pagination-bullet" data-target="4">
            <span>finzoPay</span>
            <span></span>
          </a>
          <a href="#" class="cover-pagination-bullet" data-target="5">
            <span>about</span>
            <span></span>
          </a>
        </div>
        <div class="copy-right">Finzo Co. B.S.C. (c) All Rights Reserved</div>
      </div>
      <div class="opr-container cover__slider">
        <div class="opr-page cover-wrapper opr-current first intro" data-target="1">
          <section class="cover cover_full">
            <div class="col__bg__content">
              <video autoplay muted loop>
                <source src="video/Finzo-Video.mp4" type="video/mp4">
              </video>
            </div>
            <div class="cover__overlay main__cover animatable">
              <h1>Partner with us to grow your Fintech Footprint!</h1>
              <div class="scroll-call opr-next cursor">
                <span class="scroll-call__text">Scroll</span>
                <span class="scroll-call__line"></span>
              </div>
            </div>
          </section>
        </div>
        <div class="opr-page fourth bg__one opr-common" data-target="2">
          <div class="row two_half">
            <div class="col-12 col-md-6 col__bg first-slide">
              <div class="col__bg__content">
                <div class="col__bg__wrapper" id="parallaxThird">
                  <figure class="px-layer-1" data-depth="0.20" data-src="images/FINZO_DIGITALWALLET.jpg"></figure>
                </div>
                <div class="scroll-call opr-next cursor">
                  <span class="scroll-call__text">Scroll</span>
                  <span class="scroll-call__line"></span>
                </div>
                <div class="buttons finetech-btn animatable">
                  <a class="button button_with-icon button_normal" href="#">
                    <span>Fintech Innovation in a Box</span>
                    <i class="circle__move"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col__text second-slide">
              <div class="col__text__content">
                <div class="col__text__content_main animatable">
                  <div class="animatable">
                    <div class="large__text">02</div>
                    <h2>Agile scalable feature rich wallet platform</h2>
                    <h4>with host of features to amplify user experience and utility</h4>
                  </div>
                  <ul class="animatable">
                    <li>Frictionless eKYC & onboarding</li>
                    <li>Multiple top up options</li>
                    <li>Peer to Peer (P2P) transfers</li>
                    <li>Peer to Merchant (P2M) transfers</li>
                    <li>Tap & Pay and QR based payments</li>
                    <li>Integration with 3rd party digital assets</li>
                  </ul>
                </div>
                <div class="col__text__content_sub">
                  <div class="buttons readmore-btn animatable">
                    <a class="button button_with-icon button_normal" href="{{ URL::asset('/home/solutions/digital-wallet') }}">
                      <span>read more</span>
                      <i class="circle__move"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="main__header">
            <div class="head__line animatable">
              <svg class="tagline" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="200" width="200" viewBox="3 10 54 50">
                <path d="M5,10   l50,0  0,50  -50,0  0,-50" stroke-width="1" />
              </svg>
              <h1>DIGITAL WALLET</h1>
            </div>
          </div>
        </div>
        <div class="opr-page second bg__three opr-common" data-target="3">
          <div class="row two_half">
            <div class="col-12 col-md-6 col__bg first-slide">
              <div class="col__bg__content">
                <div class="col__bg__wrapper" id="parallaxFirst">
                  <figure class="px-layer-1" data-depth="0.10" data-src="images/DigitalBanking_HOME.png"></figure>
                  <!-- <figure class="px-layer-2" data-depth="0.30" data-src="images/digital_banking.png"></figure> -->
                </div>
                <div class="scroll-call opr-next cursor">
                  <span class="scroll-call__text">Scroll</span>
                  <span class="scroll-call__line"></span>
                </div>
                <div class="buttons finetech-btn animatable">
                  <a class="button button_with-icon button_normal" href="#">
                    <span>Fintech Innovation in a Box</span>
                    <i class="circle__move"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col__text second-slide">
              <div class="col__text__content">
                <div class="col__text__content_main animatable">
                  <div class="animatable">
                    <div class="large__text">03</div>
                    <h2>Enabling banks to embrace digital banking as a service</h2>
                    <h4>& personalized end user experience</h4>
                  </div>
                  <ul class="animatable">
                    <li>Fully Functional Sandbox Environment</li>
                    <li>Open Banking/API based Platform</li>
                    <li>Modularized Composable Architecture</li>
                    <li>Data Driven Architecture</li>
                    <li>Best in Class Customer Journeys </li>
                  </ul>
                </div>
                <div class="col__text__content_sub">
                  <div class="buttons readmore-btn animatable">
                    <a class="button button_with-icon button_normal" href="{{ URL::asset('/home/solutions/digital-banking') }}">
                      <span>read more</span>
                      <i class="circle__move"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="main__header">
            <div class="head__line animatable">
              <svg class="tagline" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="200" width="200" viewBox="3 10 54 50">
                <path d="M5,10   l50,0  0,50  -50,0  0,-50" stroke-width="1" />
              </svg>
              <h1>Digital banking</h1>
            </div>
          </div>
        </div>
        <div class="opr-page third bg__two opr-common" data-target="4">
          <div class="row two_half">
            <div class="col-12 col-md-6 col__bg first-slide">
              <div class="col__bg__content">
                <div class="col__bg__wrapper" id="parallaxSecond">
                  <figure class="px-layer-1" data-depth="0.10" data-src="images/FINZOPAY_HOMEPAGE.jpg"></figure>
                  <!-- <figure class="px-layer-2" data-depth="0.30" data-src="images/finzo_pay.png"></figure> -->
                </div>
                <div class="scroll-call opr-next cursor">
                  <span class="scroll-call__text">Scroll</span>
                  <span class="scroll-call__line"></span>
                </div>
                <div class="buttons finetech-btn animatable">
                  <a class="button button_with-icon button_normal" href="#">
                    <span>Fintech Innovation in a Box</span>
                    <i class="circle__move"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col__text second-slide">
              <div class="col__text__content">
                <div class="col__text__content_main animatable">
                  <div class="animatable">
                    <div class="large__text">04</div>
                    <h2>Interoperable global wallet ecosystem</h2>
                    <h4>to allow acceptance of transactions across wallets across the world.</h4>
                  </div>
                  <ul class="animatable">
                    <li>Multi-currency Wallet Support</li>
                    <li>End-to-end secure cloud based resilient payment infrastructure</li>
                    <li>Opportunity to be part of a growing wallet network</li>
                  </ul>
                </div>
                <div class="col__text__content_sub">
                  <div class="buttons readmore-btn animatable">
                    <a class="button button_with-icon button_normal" href="{{ URL::asset('/home/solutions/finzo-pay') }}">
                      <span>read more</span>
                      <i class="circle__move"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="main__header">
            <div class="head__line animatable">
              <svg class="tagline" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="200" width="200" viewBox="3 10 54 50">
                <path d="M5,10   l50,0  0,50  -50,0  0,-50" stroke-width="1" />
              </svg>
              <h1>FinzoPay</h1>
            </div>
          </div>
        </div>
        <div class="opr-page fifth bg__four opr-common" data-target="5">
          <div class="row two_half">
            <div class="col-12 col-md-6 col__bg first-slide">
              <div class="col__bg__content">
                  <div class="col__bg__wrapper" id="parallaxFourth">
                    <figure class="px-layer-1" data-depth="0.10" data-src="images/ABOUT_US_HOMEPAGE.jpg"></figure>
                    <!-- <figure class="px-layer-2" data-depth="0.30" data-src="images/about.png"></figure> -->
                  </div>
                  <div class="scroll-call opr-next cursor">
                    <span class="scroll-call__text">Scroll</span>
                    <span class="scroll-call__line"></span>
                  </div>
                  <div class="buttons finetech-btn animatable">
                    <a class="button button_with-icon button_normal" href="#">
                      <span>Fintech Innovation in a Box</span>
                      <i class="circle__move"></i>
                    </a>
                  </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col__text second-slide">
              <div class="col__text__content">
                <div class="col__text__content_main animatable">
                  <div class="animatable">
                    <div class="large__text">05</div>
                    <div class="tag__text">
                      <h5>VISION</h5>
                    </div>
                    <h2>To be the partner of choice for fintech solutions globally!</h2>
                    <div class="mission">
                      <div class="tag__text">
                        <h5>Mission</h5>
                        <p>To redefine payments & banking experiences by delivering cutting edge technology solutions.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col__text__content_sub">
                  <div class="buttons readmore-btn animatable">
                    <a class="button button_with-icon button_normal" href="{{ URL::asset('/home/about') }}">
                      <span>read more </span>
                      <i class="circle__move"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="main__header">
            <div class="head__line animatable">
              <svg class="tagline" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="200" width="200" viewBox="3 10 54 50">
                <path d="M5,10 l50,0  0,50  -50,0  0,-50" stroke-width="1" />
              </svg>
              <h1>ABOUT</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  @stop