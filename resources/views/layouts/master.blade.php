<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="author" content="Cafe du Soleil">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('favicon/favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Finzo | @yield('pageTitle')</title>
    <link href="{{ 'css/template/common.css' }}" rel="stylesheet">
    <script src="{{ 'js/template/jquery-1.12.4.min.js' }}"></script>
    {{-- <script src="{{ 'js/template/jquery.pjax.js' }}"></script> --}}
    <style type="text/css">
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body class="inner-page about" id="pjax-container">
    <div id="application" class="application">
    <header class="header">
      <div class="header__menu">
		<div class="header__menu">
			<a class="header__logo" href="index.html">
				<img src="images/logo.png" alt>
			</a>
			<div class="header__menu-wrapper">
				<div class="hamburger hamburger--trigger cursor">
				<div class="hamburger__bar"></div>
				<div class="hamburger__bar"></div>
				<div class="hamburger__bar"></div>
				</div>
				<div class="menu_aside">
				<div class="buttons close-btn">
					<a class="button button_with-icon button_normal" href="#">
					<span>Close</span>
					<i class="circle__move"></i>
					</a>
				</div>
				<div class="menu__inner">
					<nav id="ml-menu" class="menu">
					<div class="menu__wrap">
						<ul class="menu__level opr-nav">
						<li class="menu__item"><a href="about.html" class="menu__link">About</a></li>
						<li class="menu__item"><a href="digital-wallet.html" class="menu__link">Solutions</a></li>
						<li class="menu__item"><a href="services.html" class="menu__link menu__active">Services</a></li>
						<li class="menu__item"><a href="contact.html" class="menu__link">Contact</a></li>
						</ul>
					</div>
					</nav>
					<div class="menu__bottom">
					<div class="copy-right">
						<span>Finzo Co. B.S.C. (c) All Rights Reserved</span>
					</div>
					<div class="buttons finetech-btn animatable">
						<a class="button button_with-icon button_normal" href="">
						<span>Fintech Innovation in a Box</span>
						<i class="circle__move"></i>
						</a>
					</div>
					</div>
				</div>
				</div>
			</div>
		</div>
      </div>
    </header>
    <!-- Content -->
       @yield('content')
        <!-- Portrait Mode Only -->
        <div class="portrait__mode"><p>Please Use Portrait Mode</p></div>
      </div>
    <script src="{{ 'js/template/parallax.js' }}"></script>
    <script src="{{ 'js/template/common.js' }}"></script>
    <script src="{{ 'js/template/jquery.scrollify.js' }}"></script>
    <script>
      if (window.matchMedia("(min-width: 768px)").matches){
        $(function() {
          $.scrollify({
            section : ".devider-section",
            interstitialSection : ".devider-section-interstial",
            scrollSpeed:1300,
            setHeights:false,
            offset : -100
          });
        });
      }
      if(document.getElementById('parallaxSecond')) {
        var secondScene = document.getElementById('parallaxSecond');
        var secondParallax = new Parallax(secondScene);
      }
      $(document).pjax('.pjax-class a', '#pjax-container');
    </script>
  </body>
</html>