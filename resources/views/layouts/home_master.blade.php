<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="author" content="Cafe du Soleil">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('favicon/favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Finzo | Fintech Innovation in a Box</title>
    <link href="{{ 'css/template/common.css' }}" rel="stylesheet">
  </head>
  <body class="landing">
    <div id="application-home" class="application">
      <!-- Preloader -->
      <section class="preloader-home preloader">
        <div class="preloader__logo">
          <img src="images/logo.png" alt="">
        </div>
        <div class="ip-loader">
          <svg class="ip-inner" width="440px" height="440px" viewBox="5 14 70 70">
            <defs>
              <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                <stop offset="0%" stop-color="rgba(251,66,0,1)" />
                <stop offset="100%" stop-color="rgba(254,121,3,1)" />
              </linearGradient>
            </defs>
            <path class="ip-loader-circlebg" d="M15,20   l50.5,0  0,50  -50,0  0,-50" stroke="url(#gradient)" stroke-width="1"/>
            <path id="ip-loader-circle-home" class="ip-loader-circle" d="M15,20   l50.5,0  0,50  -50,0  0,-50" stroke="url(#gradient)" stroke-width="1"/>
          </svg>
        </div>
      </section>
      <!-- header -->
      <header class="header">
        <div class="header__menu">
          <a class="header__logo" href="index.html">
            <img src="images/logo.png" alt>
          </a>
          <div class="header__lang">
            <a class="button button_border active" href="#"><span>En</span></a>
            <a class="button button_border" href="#"><span>Ar</span></a>
          </div>
          <div class="header__menu-wrapper">
            <div class="hamburger hamburger--trigger cursor">
              <div class="hamburger__bar"></div>
              <div class="hamburger__bar"></div>
              <div class="hamburger__bar"></div>
            </div>
            <div class="menu_aside">
              <div class="buttons close-btn">
                <a class="button button_with-icon button_normal" href="#">
                  <span>Close</span>
                  <i class="circle__move"></i>
                </a>
              </div>
              <div class="menu__inner">
                <nav id="ml-menu" class="menu">
                  <div class="menu__wrap">
                    <ul class="menu__level opr-nav">
                      <li class="menu__item"><a href="{{ URL::asset('/home/about') }}" class="menu__link">About</a></li>
                      <li class="menu__item"><a href="{{ URL::asset('/home/solutions/digital-wallet') }}" class="menu__link">Solutions</a></li>
                      <li class="menu__item"><a href="{{ URL::asset('/home/services') }}" class="menu__link">Services</a></li>
                      <li class="menu__item"><a href="{{ URL::asset('/home/contact') }}" class="menu__link">Contact</a></li>
                    </ul>
                  </div>
                </nav>
                <div class="menu__bottom">
                  <div class="copy-right">
                    <span>Finzo Co. B.S.C. (c) All Rights Reserved</span>
                  </div>
                  <div class="buttons finetech-btn animatable">
                    <a class="button button_with-icon button_normal" href="">
                      <span>Fintech Innovation in a Box </span>
                      <i class="circle__move"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- Container fluid  -->
       @yield('content')
    </div>
    <script src="{{ URL::asset('js/template/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ URL::asset('js/template/vendors.min.js') }}"></script>
    <script src="{{ URL::asset('js/template/common.js') }}"></script>
    <script>
      if (window.matchMedia("(min-width: 768px)").matches) {
        var firstScene = document.getElementById('parallaxFirst');
        var firstParallax = new Parallax(firstScene);
        var secondScene = document.getElementById('parallaxSecond');
        var secondParallax = new Parallax(secondScene);
        var thirdScene = document.getElementById('parallaxThird');
        var thirdParallax = new Parallax(thirdScene);
        var fourthScene = document.getElementById('parallaxFourth');
        var fourthParallax = new Parallax(fourthScene);
      }
      (function() {
        var support = { animations : Modernizr.cssanimations },
        container = document.getElementById( 'application-home' ),
        header = container.querySelector( '.preloader-home' ),
        loader = new PathLoader( document.getElementById( 'ip-loader-circle-home' ) ),
        animEndEventNames = { 'WebkitAnimation' : 'webkitAnimationEnd', 'OAnimation' : 'oAnimationEnd', 'msAnimation' : 'MSAnimationEnd', 'animation' : 'animationend' },
        animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ];

      function init() {
        var onEndInitialAnimation = function() {
          if( support.animations ) {
            this.removeEventListener( animEndEventName, onEndInitialAnimation );
          }

          startLoading();
        };
        window.addEventListener( 'scroll', noscroll );
        classie.add( container, 'loading' );

        if( support.animations ) {
          container.addEventListener( animEndEventName, onEndInitialAnimation );
        }
        else {
          onEndInitialAnimation();
        }
      }
      function startLoading() {
        var simulationFn = function(instance) {
          var progress = 0,
            interval = setInterval( function() {
              progress = Math.min( progress + Math.random() * 0.1, 1 );

              instance.setProgress( progress );
              if( progress === 1 ) {
                classie.remove( container, 'loading' );
                classie.add( container, 'loaded' );
                clearInterval( interval );

                var onEndHeaderAnimation = function(ev) {
                  if( support.animations ) {
                    if( ev.target !== header ) return;
                    this.removeEventListener( animEndEventName, onEndHeaderAnimation );
                  }

                  classie.add( document.body, 'layout-switch' );
                  window.removeEventListener( 'scroll', noscroll );
                  $(".intro h1").each(function(e) {
                                    $(this).css("animation", "fadeInUp 1s forwards");
                                });
                };
                if( support.animations ) {
                  header.addEventListener( animEndEventName, onEndHeaderAnimation );
                }
                else {
                  onEndHeaderAnimation();
                }
              }
            }, 80 );
        };

        loader.setProgressFn( simulationFn );
      }
      function noscroll() {
        window.scrollTo( 0, 0 );
      }
      init();
      })();
    </script>
  </body>
</html>