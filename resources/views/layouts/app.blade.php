<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Finzo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/ionicons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/themify-icons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/header.css" rel="stylesheet">
    <link href="/assets/css/menu.css" rel="stylesheet">
    <link href="/assets/css/index.css" rel="stylesheet">
    <link href="/assets/css/responsive.css" rel="stylesheet">
    <style>
       .img-logo {
          width: 50px;
          height: 100%;
       }
    </style>
</head>

<body class="bg_darck">
  <div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
      <div class="login-content">
        <div class="logo">
          <a href="#">
            <span class="logo-default">
              {{-- <img alt="" src="/assets/images/logo.png" class="img-logo"> --}}
            </span>
          </a>
        </div>
        @yield('content')
      </div>
    </div>
  </div>

    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/popper.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/assets/js/custom.js" type="text/javascript"></script>
</body>
</html>