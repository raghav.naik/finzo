@extends('admin.app')
@section('content')
    @if(session()->has('alert-success'))
        <div class="alert alert-success">
            {{ session()->get('alert-success') }}
        </div>
    @endif
<div class="row">
    <div class=" col-xl-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            Permission List
                        </div>
                        @can('permissions.create')
                        <div class="col-sm-12 col-md-6 text-right">
                            <a href="/admin/permissions/create" class="btn btn-info">Add Permission</a>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Name</th>
                            @if(Auth::user()->hasAnyPermission(['permissions.edit', 'permissions.destroy']))
                            <th scope="col">Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissions as $permission)
                        <tr>
                            <th scope="row">{{$permission->id}}</th>
                            <td>{{$permission->title}}</td>
                            <td>{{$permission->name}}</td>


                            <td>
                                @can('permissions.edit')
                                <a href="/admin/permissions/{{$permission->id}}/edit" class="btn btn-dark btn-sm"><i class="icon-note "></i></a>
                                @endcan
                                @can('permissions.destroy')
                                <form action="{{url('/admin/permissions',$permission->id)}}" method="POST">
                                    {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn btn-dark btn-sm"><i class="fa fa-trash-o"></i></button></form>                            
                                @endcan
                            </td>

                        </tr>
                        @endforeach
                        @if(count($permissions) == 0)
                        <tr>
                            <td colspan="5">
                            {{"No Permissions Found"}}
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                {{ $permissions->onEachSide(1)->links() }}
            </div>
        </div>
    </div>
</div>

@endsection