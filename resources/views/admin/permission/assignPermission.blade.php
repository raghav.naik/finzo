
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
@extends('admin.app')

@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Assign Permission to Roles
                </div>
            </div>
            <div class="card-body">

                   <div class="card card-shadow mb-4">
                        <div class="card-header">
                            <div class="card-title">
                                Roles
                            </div>
                        </div>
                        <div class="form-group">
                            <form method="POST" action="{{url('admin/permission/assign-permission')}}" id="inner_form" >
                            @csrf 
                                <div class="card-body">
                                    <select id="filter_type" name="role" class="form-control col-sm-5" onchange="this.form.submit()">
                                          @if(isset($roles)) 
                                            @foreach($roles as $role)
                                                <option value={{$role->id}} @if(isset($role->selected))
                                        @if($role->selected == true) selected @endif @endif>{{$role->title}}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </form>
                        </div>
                  </div>
                    <form method="POST" class="right-text-label-form" action="{{url('admin/permission/save')}}">
                    @csrf
                   <div class="card card-shadow mb-4">
                        <div class="card-header">
                            <div class="card-title">
                                Permissions
                            </div>
                        </div>
                        @if(isset($newRole))
                            <input type="hidden" name="role" value="{{$newRole}}">
                        @else
                            <input type="hidden" name="role" value="1">
                        @endif        
                        <div class="card-body">
                            <div class="row">
                           @if(isset($oldPermissions))
                                @foreach($oldPermissions as $key => $oldPermission)                                    
                                    <div class="col-md-3">                                        
                                        <label class="control control-outline control--checkbox" for="'customCheck'.{{$oldPermission->id}}" >{{$oldPermission->title}}
                                        <input type="checkbox" id="'customCheck'.{{$oldPermission->id }}" name="permission[]" value={{$oldPermission->id }}
                                        @if(isset($oldPermission->checked)) @if($oldPermission->checked==1) checked @endif @endif>
                                        <span class="control__indicator"></span> </label>
                                        @if ($errors->has('permission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('permission') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    @can('assign.roles.permission')
                    <div class="form-group row">
                          <div class="card-body">
                            <button type="submit" class="btn btn-success">
                               Save
                            </button>
                        </div>
                    </div>
                    @endcan
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
