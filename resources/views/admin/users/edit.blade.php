@extends('admin.app')
@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Edit User
                     <span class="col-sm-12 col-md-6 text-right">
                       <a href="{{url('admin/permission/user-permission')}}/{{$user->id}}" class="btn btn-info">Permission</a>
                    </span>
                </div>
               
            </div>
            <div class="card-body">
				<form method="POST" class="right-text-label-form"  action="/admin/users/{{$user->id}}">
                    @method('PATCH')
                    @csrf
                        <div class="form-group row {{ $errors->has('first_name') ? 'has-danger' : '' }}">
                        <label for="first_name" class="col-sm-4 col-form-label">First Name</label>
                            <div class="col-sm-5">
                                <input id="first_name" type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{old('last_name', $user->first_name)}}"  placeholder="First Name" autofocus>

                                @if ($errors->has('first_name'))
                                     <small class="form-text">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('last_name') ? 'has-danger' : '' }}">
                            <label for="last_name" class="col-sm-4 col-form-label">Last Name</label>
                                <div class="col-sm-5">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"  name="last_name" value="{{old('last_name', $user->last_name)}}"  placeholder="First Name" autofocus>
                                @if ($errors->has('last_name'))
                                     <small class="form-text">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('username') ? 'has-danger' : '' }}">
                            <label for="username" class="col-sm-4 col-form-label">User Name</label>
                                <div class="col-sm-5">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{old('username', $user->username)}}"  placeholder="User Name">
                                @if ($errors->has('username'))
                                     <small class="form-text">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('email') ? 'has-danger' : '' }}">
                            <label for="email" class="col-sm-4 col-form-label">E-mail</label>
                                <div class="col-sm-5">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email', $user->email)}}"  placeholder="E-Mail Address">

                                @if ($errors->has('email'))
                                     <small class="form-text">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('phone') ? 'has-danger' : '' }}">
                            <label for="phone" class="col-sm-4 col-form-label">Phone</label>
                                <div class="col-sm-5">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{old('phone', $user->phone)}}"  placeholder="Phone Number">
                                @if ($errors->has('phone'))
                                     <small class="form-text">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('role') ? 'has-danger' : '' }}">
                            <label for="role" class="col-sm-4 col-form-label">Roles</label>
                                <div class="col-sm-5">
									@foreach($roles as $role)
										<label class="text-left control col-sm-6 control--checkbox" for="'customCheck'.{{$role->id}}" >{{$role->title}}
										<input type="checkbox" id="'customCheck'.{{$role->id }}" name="roles[]" value="{{$role->id }}" {{ (is_array(old('roles')) && in_array($role->id, old('roles'))) ? ' checked' : $user->hasRole($role->name) ? 'checked' : '' }}  ><span class="control__indicator"></span> </label>

									 @if ($errors->has('roles'))
                                      <small class="form-text">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </small>
                                @endif
									@endforeach
								</div>
							</div>

                        <div class="form-group row {{ $errors->has('gender') ? 'has-danger' : '' }}">
                            <label for="gender" class="col-sm-4 col-form-label">Gender</label>
                                <div class="col-sm-5">
                                <select name="gender" class="form-control">
                                    <option value="1" @if ($user->gender == '1') selected="selected" @endif>Male</option>
                                    <option value="2" @if ($user->gender == '2') selected="selected" @endif>Female</option>
                                </select>
                                 @if($errors->has('gender'))
                                     <small class="form-text">
                                        <strong>{{$errors->first('gender')}}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('s') ? 'has-danger' : '' }}">
                            <label for="status" class="col-sm-4 col-form-label">Status</label>
                                <div class="col-sm-5">
                                <select name="status" class="form-control" >
                                    <option value="0" @if ($user->status == '0') selected="selected" @endif>Disabled</option>
                                    <option value="1" @if ($user->status == '1') selected="selected" @endif>Active</option>
                                </select>
                                @if($errors->has('status'))
                                     <small class="form-text">
                                        <strong>{{$errors->first('status')}}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>

                            <div class="form-group row">
                                <div class="col-sm-8 ml-auto">
                                    <button type="submit" class="btn btn-success">
                                    Update
                                    </button>
                                </div>
                            </div>
                       </form>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>





@endsection