@extends('admin.app')
@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Assign Permission to User
                </div>
            </div>
            <div class="card-body">
                    <form method="POST" class="right-text-label-form" action="{{url('admin/users/save')}}">
                    @csrf 
                   <div class="card card-shadow mb-4">
                        <div class="card-header">
                            <div class="card-title">
                                Permissions
                            </div>
                        </div>
                         @if(isset($userId))
                            <input type="hidden" name="user" value="{{$userId}}">
                        @endif        
                        <div class="card-body">
                            @if(isset($oldPermissions))
                                @foreach($oldPermissions as $oldPermission)
                                 
                                    <label class="control control-outline control--checkbox"  for="'customCheck'.{{$oldPermission->id}}" >{{$oldPermission->title}}
                                    <input type="checkbox"  id="'customCheck'.{{$oldPermission->id }}" name="permission[]" value="{{$oldPermission->id }}" 
                                    @if(isset($oldPermission->checked))
                                    @if($oldPermission->checked==1) checked  @endif @endif 
                                    @if(isset($oldPermission->disabled))
                                    @if($oldPermission->disabled==1) disabled  @endif @endif >
                                    <span class="control__indicator"></span> </label>
                                    @if ($errors->has('permission'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('permission') }}</strong>
                                        </span>
                                    @endif  

                                @endforeach
                            @endif 
                        </div>
                    </div>     
                    <div class="form-group row">
                        <div class="card-body">
                            <button type="submit" class="btn btn-success">
                               Save
                            </button>
                        </div>
                    </div>
                </form>
                    
            </div>
        </div>
    </div>
</div>
@endsection
