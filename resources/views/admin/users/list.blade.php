@extends('admin.app')
@section('content')
    @if(session()->has('alert-success'))
        <div class="alert alert-success">
            {{ session()->get('alert-success') }}
        </div>
    @endif
<div class="row">
    <div class=" col-xl-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                             Users List
                        </div>
                         @can('users.create')
                        <div class="col-sm-12 col-md-6 text-right">
                            <a href="/admin/roles/create" class="btn btn-info">Add User</a>
                        </div>
                        @endrole
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#ID</th>
                            <th scope="col">User Name</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Status</th>
                            @if(Auth::user()->hasAnyPermission(['users.edit', 'users.destroy']))
                            <th scope="col">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->gender == '1' ? 'Male' : 'Female'}}</td>
                                <td>{{$user->status == '1' ? 'Active' : 'In Active'}}</td>
                            <td>
                            @can('users.edit')
                            <a href="/admin/users/{{$user->id}}/edit" class="btn btn-dark btn-sm"><i class="icon-note "></i></a>
                            @endcan
                            @can('users.destroy')
                            <form action="{{url('/admin/users',$user->id)}}" method="POST">

                                {{ method_field('DELETE') }}
                              {{ csrf_field() }}
                            <button class="btn btn-dark btn-sm"><i class="fa fa-trash-o"></i></button></form>
                            @endcan
                            </td>
                        </tr>
                        @endforeach
                        @if(count($users) == 0)
                        <tr>
                            <td colspan="5">
                            {{"No Roles Found"}}
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                  {{ $users->onEachSide(1)->links() }}
            </div>
        </div>
    </div>
</div>

@endsection


