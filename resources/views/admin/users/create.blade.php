@extends('admin.app')
@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Add User
                </div>
            </div>
			<div class="card-body">
				<form method="POST" class="right-text-label-form"  action="{{url('admin/users')}}">
                    @csrf
                    <div class="form-group row {{ $errors->has('first_name') ? 'has-danger' : '' }}">
                        <label for="first_name" class="col-sm-4 col-form-label">First Name</label>
                            <div class="col-sm-5">
                                <input id="first_name" type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}"  placeholder="First Name" autofocus>
                                 @if ($errors->has('first_name'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('last_name') ? 'has-danger' : '' }}">
                            <label for="last_name" class="col-sm-4 col-form-label">Last Name</label>
                            <div class="col-sm-5">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}"  placeholder="Last Name" autofocus>
                                @if ($errors->has('last_name'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('username') ? 'has-danger' : '' }}">
                            <label for="username" class="col-sm-4 col-form-label">User Name</label>
                            <div class="col-sm-5">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}"  placeholder="User Name">
                                @if ($errors->has('username'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('email') ? 'has-danger' : '' }}">
                        <label for="email" class="col-sm-4 col-form-label">E-mail</label>
                            <div class="col-sm-5">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  placeholder="E-Mail Address">
                                @if ($errors->has('email'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('phone') ? 'has-danger' : '' }}">
                            <label for="phone" class="col-sm-4 col-form-label">Phone</label>
                            <div class="col-sm-5">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}"  placeholder="Phone Number">
                                @if ($errors->has('phone'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('password') ? 'has-danger' : '' }}">
                            <label for="password" class="col-sm-4 col-form-label">Password</label>
                                <div class="col-sm-5">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  placeholder="Password">
                                    @if ($errors->has('password'))
                                        <small class="form-text">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </small>
                                    @endif
                                </div>
                            </div>
                        <div class="form-group row {{ $errors->has('password_confirmation') ? 'has-danger' : '' }}">
                            <label for="password_confirmation" class="col-sm-4 col-form-label">Confirm Pssword</label>
                            <div class="col-sm-5">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password" >
                            </div>
                        </div>
						<div class="form-group row {{ $errors->has('role') ? 'has-danger' : '' }}">
                            <label for="role" class="col-sm-4 col-form-label">Roles</label>
                                <div class="col-sm-5">
									@foreach($roles as $role)
                                    <label class="text-left  control col-sm-6 control--checkbox " for="'customCheck'.{{$role->id}}" >{{$role->name}}
									<input type="checkbox" id="'customCheck'.{{$role->id }}" name="roles[]" value={{$role->id }} {{ (is_array(old('roles')) && in_array($role->id, old('roles'))) ? ' checked' : '' }} >
									<span class="control__indicator"></span> </label>
    								@if ($errors->has('role'))
                                        <small class="form-text">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </small>
                                    @endif
								@endforeach
							</div>
						</div>
                        <div class="form-group row {{ $errors->has('gender') ? 'has-danger' : '' }}">
                            <label for="gender" class="col-sm-4 col-form-label">Gender</label>
                                <div class="col-sm-5">
                                <select name="gender" class="form-control">
                                    <option value="1" @if (Input::old('gender') == '1') selected="selected" @endif>Male</option>
                                    <option value="2" @if (Input::old('gender') == '2') selected="selected" @endif>Female</option>
                                </select>
                                @if ($errors->has('gender'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('s') ? 'has-danger' : '' }}">
                            <label for="status" class="col-sm-4 col-form-label">Status</label>
                            <div class="col-sm-5">
                                <select name="status" class="form-control" >
                                    <option  value="1" @if (Input::old('status') == '1') selected="selected" @endif>Active</option>
                                    <option value="0" @if (Input::old('status') == '0') selected="selected" @endif>Disabled</option>
                                </select>
                                @if ($errors->has('status'))
                                    <small class="form-text">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </small>
                                @endif
                            </div>
                        </div>
                            <div class="form-group row">
                                <div class="col-sm-8 ml-auto">
                                    <button type="submit" class="btn btn-success">
                                    Add User
                                    </button>
                                </div>
                            </div>
                       </form>
                    </div>
            	</div>
        	</div>
    	</div>

@endsection

