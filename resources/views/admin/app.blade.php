<!DOCTYPE html>
<html lang="en">
  <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Finzo</title>
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/css/ionicons.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('assets/css/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/weather-icons.min.css') }}" rel="stylesheet">
		<!--Morris Chart -->
		<link rel="stylesheet" href="{{ asset('assets/js/index/morris-chart/morris.css') }}">

		<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/header.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/index.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/dark_theme.css') }}" rel="stylesheet">

		<!--Date Picker CSS-->
		<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

		<style>
		.img-logo {
			width: 50px;
			height: 100%;
		}
		.sidebar-menu li a.active {
			background: #1b1f24;
			border-left: 3px solid #fff;
		}
		td form {
			padding: 0;
			margin: 0;
			display: inline;
		}
		</style>
  </head>
  <body>
      <div class="wrapper">
          @include('admin.layouts.header')
          <div class="container_full">
              @include('admin.layouts.sidebar')
              <main class="content_wrapper">
                @include('admin.layouts.page_header')
                <div class="container-fluid">
                  @yield('content')
                </div>
              </main>
          </div>
      </div>

      <!--Date Picker Script-->
      <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
      <script>
          $('.expiry_date').datepicker({
              format: 'yyyy-mm-dd'
          });
      </script>

      <!--Date Picker Script End-->


      <script type="text/javascript" src="{{ asset('assets/js/popper.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/jquery.dcjqaccordion.2.7.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/custom.js') }}" ></script>
      <!-- include summernote css/js-->
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

      <script>
      $(document).ready(function() {
        $('.summernote').summernote({
          height:300,
        });
      });
      </script>
  </body>
</html>