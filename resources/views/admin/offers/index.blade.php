@extends('admin.app')
@section('content')

<div class="row">
   <div class=" col-xl-12">
      <div class="card card-shadow mb-4">
        <div class="card-header">
          <div class="card-title">
            <div class="row">
              <div class="col-sm-12 col-md-6">Offers List</div>
			  	@can('offers.create')
                <div class="col-sm-12 col-md-6 text-right">
                  <a href="/admin/offers/create" class="btn btn-info">Add Offers</a>
                </div>
				@endcan
             </div>
            </div>
         </div>
    
	<div class="card-body">
		  @if(session()->get('success'))
    		<div class="alert alert-success">
      		{{ session()->get('success') }}  
    		</div><br />
  		@endif
		<table class="table">
			<thead>
				<tr>
					<th scope="col">#ID</th>
					<th scope="col">Image</th>
					<th scope="col">Title</th>
					<th scope="col">Expiry Date</th>
					@if(Auth::user()->hasAnyPermission(['offers.create', 'offers.destroy']))
					<th scope="col">Actions</th>	
					@endcan				
				</tr>
			</thead>
			<tbody>
				@foreach($offers as $offer)
				<tr>
					<th scope="row">{{$offer->id}}</th>
					<td><img src="{{$offer->file->file_path}}" height="50px" width="50px" /></td>
					<td>{{$offer->title}}</td>
					<td>{{$offer->expiry_date}}</td>
					<td>
					@can('pages.edit')
					<a href="{{ route('offers.edit',$offer->id)}}" class="btn btn-square btn-dark btn-sm"><i class="icon-note "></i></a>					
					@endcan
					@can('pages.destroy')
						<form action="{{ route('offers.destroy', $offer->id)}}" method="post">
			              @csrf
			              @method('DELETE')
			              <button class="btn btn-square btn-secondary btn-sm" type="submit"><i class="icon-trash "></i></button>
			            </form>
					@endcan
					</td>
				</tr>
				 @endforeach
          @if(count($offers) == 0)
          <tr>
              <td colspan="5">
              {{"No Offers Found"}}
              </td>
          </tr>
          @endif
			</tbody>	
		</table>	
		<div class="row">
			<div class="col-sm-12 col-md-5">
				<div class="dataTables_info" id="bs4-table_info" role="status" aria-live="polite">Showing 1 to 10 of {{ $offers->total() }} entries
				</div>
			</div>
			<div class="col-sm-12 col-md-7">
			<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
				<!--Pagination-->
				{{ $offers->links() }}
			</div>
		</div>
	</div>

	</div>
</div>
</div>
</div>
@endsection