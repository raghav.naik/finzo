@extends('admin.app')
@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Add Offers
                </div>
            </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('offers.store') }}" class="right-text-label-form" >
        @csrf
       <div class="form-group row {{ $errors->has('title') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Title</label>
          <div class="col-sm-5">
              <input type="text" class="form-control {{ $errors->has('title') ? 'has-input' : '' }}" name="title" id="title" placeholder="Title" value="{{old('title')}}">
              @if ($errors->has('title'))
                  <small class="form-text">
                      <strong>{{ $errors->first('title') }}</strong>
                  </small>
              @endif
          </div>
        </div>

        <div class="form-group row {{ $errors->has('description') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Description</label>
            <div class="col-sm-5">
                <textarea type="text" class="form-control {{ $errors->has('description') ? 'has-input' : '' }}" name="description" id="description" placeholder="Description" value="{{old('description')}}" rows="3"></textarea>
                @if ($errors->has('description'))
                    <small class="form-text">
                        <strong>{{ $errors->first('description') }}</strong>
                    </small>
                @endif
            </div>
        </div>

        <div class="form-group row {{ $errors->has('file_id') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Select Image</label>
          <div class="col-sm-5">  
            <div class="input-group">
              <span class="input-group-btn">
                <a id="lfm" data-input="file_id" data-preview="holder2" class="btn btn-info btn-lg text-white">
                  <i class="fa fa-picture-o"></i> Choose
                </a>
              </span>
            <input id="file_id" class="form-control" type="text" name="file_id">
            </div>
            <div id="holder2" style="margin-top:15px;max-height:100px;"></div>
              @if ($errors->has('file_id'))
                  <small class="form-text">
                      <strong>{{ $errors->first('file_id') }}</strong>
                  </small>
              @endif
          </div>
        </div>

        <div class="form-group row {{ $errors->has('expiry_date') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Expiry Date</label>
            <div class="col-sm-5">
                <input type="text" class="expiry_date form-control {{ $errors->has('expiry_date') ? 'has-input' : '' }}" name="expiry_date" id="expiry_date" placeholder="Expiry Date" value="{{old('expiry_date')}}">
                @if ($errors->has('expiry_date'))
                    <small class="form-text">
                        <strong>{{ $errors->first('expiry_date') }}</strong>
                    </small>
                @endif
            </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-8 ml-auto">
              <button type="submit" class="btn btn-success">
                  Create Offer
              </button>
          </div>
        </div>
      </form>
  </div>
</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script>
   var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
  </script>

  <script>
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
  </script>
  <script>
    $('#lfm').filemanager('', {prefix: route_prefix});
  </script>


  <style>
    .popover {
      top: auto;
      left: auto;
    }
  </style>
@endsection