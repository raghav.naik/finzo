<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Finzo</title>
      <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
      <!-- google font -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
      <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('assets/css/ionicons.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('assets/css/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/weather-icons.min.css') }}" rel="stylesheet">
      <!--Morris Chart -->
      <link rel="stylesheet" href="{{ asset('assets/js/index/morris-chart/morris.css') }}">

      <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/header.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/index.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
      <link href="{{ asset('assets/css/dark_theme.css') }}" rel="stylesheet">

        <!--File manager-->
        <link rel="shortcut icon" type="image/png" href="{{ asset('vendor/laravel-filemanager/img/72px color.png') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
        <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/cropper.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/dropzone.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/mime-icons.min.css') }}">
        <style>{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/css/lfm.css')) !!}</style>
        {{-- Use the line below instead of the above if you need to cache the css. --}}
        {{-- <link rel="stylesheet" href="{{ asset('/vendor/laravel-filemanager/css/lfm.css') }}"> --}}
        <style>
          .img-logo {
              width: 50px;
              height: 100%;
          }
        </style>
    </head>
    <body>
    <?php if(basename($_SERVER['REQUEST_URI']) == 'laravel-filemanager') { ?>
      <div class="wrapper">
          @include('admin.layouts.header')
          <div class="container_full">
              @include('admin.layouts.sidebar')
              <div class="content_wrapper bg_homebefore">
                <div class="container-fluid">
                  @yield('content')
                </div>
              </div>
          </div>
      </div>
    <?php } else { ?>
        <div class="content_wrapper bg_homebefore">
            <div class="container-fluid">
              @yield('content')
            </div>
        </div>
    <?php } ?>

      <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/popper.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
      <script src="{{ asset('assets/js/index/Chart.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/jquery.dcjqaccordion.2.7.js') }}"></script>
      <script src="{{ asset('assets/js/custom.js') }}" type="text/javascript"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
      <script src="{{ asset('vendor/laravel-filemanager/js/cropper.min.js') }}"></script>
      <script src="{{ asset('vendor/laravel-filemanager/js/dropzone.min.js') }}"></script>
      <script>
          var lfm_route = "{{ url(config('lfm.url_prefix')) }}";
          var lang = {!! json_encode(trans('laravel-filemanager::lfm')) !!};
          var actions = [
            // {
            //   name: 'use',
            //   icon: 'check',
            //   label: 'Confirm',
            //   multiple: true
            // },
            {
              name: 'rename',
              icon: 'edit',
              label: lang['menu-rename'],
              multiple: false
            },
            {
              name: 'download',
              icon: 'download',
              label: lang['menu-download'],
              multiple: true
            },
            // {
            //   name: 'preview',
            //   icon: 'image',
            //   label: lang['menu-view'],
            //   multiple: true
            // },
            {
              name: 'move',
              icon: 'paste',
              label: lang['menu-move'],
              multiple: true
            },
            {
              name: 'resize',
              icon: 'arrows-alt',
              label: lang['menu-resize'],
              multiple: false
            },
            {
              name: 'crop',
              icon: 'crop',
              label: lang['menu-crop'],
              multiple: false
            },
            {
              name: 'trash',
              icon: 'trash',
              label: lang['menu-delete'],
              multiple: true
            },
          ];

          var sortings = [
            {
              by: 'alphabetic',
              icon: 'sort-alpha-down',
              label: lang['nav-sort-alphabetic']
            },
            {
              by: 'time',
              icon: 'sort-numeric-down',
              label: lang['nav-sort-time']
            }
          ];
      </script>
      <script>{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/script.js')) !!}</script>
      {{-- Use the line below instead of the above if you need to cache the script. --}}
      {{-- <script src="{{ asset('vendor/laravel-filemanager/js/script.js') }}"></script> --}}
      <script>
          Dropzone.options.uploadForm = {
            paramName: "upload[]", // The name that will be used to transfer the file
            uploadMultiple: false,
            parallelUploads: 5,
            clickable: '#upload-button',
            dictDefaultMessage: "{{ trans('laravel-filemanager::lfm.message-drop') }}",
            init: function() {
              var _this = this; // For the closure
              this.on('success', function(file, response) {
                if (response == 'OK') {
                  loadFolders();
                } else {
                  this.defaultOptions.error(file, response.join('\n'));
                }
              });
            },
            headers: {
              'Authorization': 'Bearer {{ request('token') }}'
            },
            acceptedFiles: "{{ implode(',', $helper->availableMimeTypes()) }}",
            maxFilesize: ({{ $helper->maxUploadSize() }} / 1000)
          }
        </script>
  </body>
</html>