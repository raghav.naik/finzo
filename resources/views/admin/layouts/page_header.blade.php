<div class="page-heading">
    <div class="container-fluid">
        <div class="row d-flex align-items-center">
            <div class="col-md-6">
                <div class="page-breadcrumb">
                @if(isset($pageHeading))
                    <h1>{{$pageHeading}}</h1>
                @endif
                </div>
            </div>
            <div class="col-md-6 justify-content-md-end d-md-flex">
                <div class="breadcrumb_nav">
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a class="parent-item" href="route({{'admin/home'}})">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">

                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>