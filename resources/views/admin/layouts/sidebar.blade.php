<div class="side_bar scroll_auto">
    <div class="user-panel">
        <div class="user_image">
            <img src="{{ asset('assets/images/about-1.jpg') }}" class="img-circle mCS_img_loaded" alt="User Image">
        </div>
        <div class="info">
            <p>
                {{ isset(Auth::user()->first_name) ? Auth::user()->first_name : '' }}
                {{ isset(Auth::user()->last_name) ? Auth::user()->last_name : '' }}
            </p>
        </div>
    </div>
    <ul id="dc_accordion" class="sidebar-menu tree">
        <li class="menu_sub">
          <a href="{{ url('admin/home') }}" class="{{ request()->is('admin/home') ? 'active' : '' }}" > <i class="ti-home"></i> <span>Dashboard</span></a>
        </li>
        @can('users.index')
            <li class="menu_sub">
                <a href="{{ url('admin/users') }}" class="{{ request()->is('admin/users') ? 'active' : '' }}"> <i class="fa fa-user"></i> <span>Users </span></a>
            </li>
        @endcan
        @can('roles.index')
        <li class="menu_sub">
            <a href="{{ url('admin/roles') }}" class="{{ request()->is('admin/roles') ? 'active' : '' }}"> <i class="fa fa-user"></i> <span>Roles</span></a>
        </li>
        @endcan

        @can('permissions.index')
        <li class=" route">
            <a href="{{ url('admin/permissions') }}" class="{{ request()->is('admin/permissions') ? 'active' : '' }}"> <i class="fa fa-users"></i> <span>Permissions </span></a>
        </li>
        @endcan

        @can('view.roles.permission')
        <li class="menu_sub">
            <a href="{{ url('admin/permission/assign-permission') }}" class="{{ request()->is('admin/permission/assign-permission') ? 'active' : '' }}"> <i class="fa fa-users"></i> <span>User Roles & Permissions </span></a>            
        </li>
        @endcan

        @can('pages.index')
        <li class="menu_sub">
            <a href="{{ url('admin/pages') }}" class="{{ request()->is('admin/pages') ? 'active' : '' }}"> <i class="fa fa-picture-o"></i> <span>Pages </span></a>

        </li>
        @endcan

        @can('logs.index')
        <li class="menu_sub">
            <a href="{{ url('admin/logs') }}" class="{{ request()->is('admin/logs') ? 'active' : '' }}"> <i class="fa fa-picture-o"></i> <span>Logs </span></a>

        </li>
        @endcan

        @hasanyrole('super_admin|admin')
        <li class="menu_sub">
            <a href="{{ url('laravel-filemanager') }}" class="{{ request()->is('/laravel-filemanager') ? 'active' : '' }}"> <i class="fa fa-picture-o"></i> <span>Gallery </span></a>
        </li>
        @endhasanyrole

        @if(Auth::user()->hasAnyPermission(['promotions.index', 'offers.index']))
        <li class="menu_sub">
            <a href="#" class="{{ (request()->is('admin/promotions') || request()->is('admin/offers')) ? 'active' : '' }}"> <i class="fa fa-table"></i> <span>Bwallet</span> <span class="icon-arrow-down styleicon"></span> </a>
            <ul class="down_menu">
                @can('promotions.index')
                <li>                    
                    <a href="{{ asset('admin/offers') }}">Promotion Offers</a>
                </li>
                @endcan
                @can('offers.index')
                <li>
                    <a href="portlet-advanced.html">Merchants Logos</a>
                </li>
                @endcan
            </ul>
        </li>
        @endif
    </ul>
</div>