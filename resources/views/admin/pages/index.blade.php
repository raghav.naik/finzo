@extends('admin.app')
@section('content')

<div class="row">
  <div class=" col-xl-12">
    <div class="card card-shadow mb-4">
      <div class="card-header">
        <div class="card-title">
          <div class="row">
            <div class="col-sm-12 col-md-6">Pages</div>
            @can('pages.create')
             	<div class="col-sm-12 col-md-6 text-right">
                <a href="/admin/pages/create" class="btn btn-info">Add Pages</a>
              </div>
            @endcan
          </div>
        </div>
     	</div>    
			<div class="card-body">
			  @if(session()->get('success'))
	    		<div class="alert alert-success">
	      		{{ session()->get('success') }}  
	    		</div><br />
	  		@endif
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#ID</th>
							<th scope="col">Title</th>
							<th scope="col">Name</th>
							<th scope="col">Page Slug</th>
							@if(Auth::user()->hasAnyPermission(['pages.create', 'pages.destroy']))
							<th scope="col">Actions</th>
							@endif
						</tr>
					</thead>
					<tbody>
					@foreach($pages as $page)
					<tr>
					<th scope="row">{{$page->id}}</th>
					<td>{{$page->title}}</td>
					<td>{{$page->name}}</td>
					<td>{{$page->url_slug}}</td>
					<td>
					@can('pages.edit')
						<a href="{{ route('pages.edit',$page->id)}}" class="btn btn-dark btn-sm">
						<i class="icon-note "></i>
						</a>
					@endcan
					@can('pages.destroy')
						<form action="{{ route('pages.destroy', $page->id)}}" method="post">
						@csrf
						@method('DELETE')
						<button class="btn btn-dark btn-sm" type="submit"><i class="icon-trash "></i></button>
						</form>
					@endcan
					</td>
					</tr>
					@endforeach
					@if(count($pages) == 0)
					<tr>
					<td colspan="5">
						{{"No Pages Found"}}
					</td>
					</tr>
					@endif
				</tbody>	
				</table>
				<div class="row">
					<div class="col-sm-12 col-md-7">
						<div class="dataTables_paginate paging_simple_numbers" id="bs4-table_paginate">
							<!--Pagination-->
							{{ $pages->links() }}
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
@endsection