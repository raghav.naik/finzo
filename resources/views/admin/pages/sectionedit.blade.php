@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Page Details </h2>
            </div>

        </div>
    </div>
    {{$section->title}}
<br>
    <div class="card card-shadow mb-4">
      <div class="card-header">
        <div class="card-title">
          Pages
        </div>
      </div>
      <div class="card-body">
        @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div><br />
        @endif
        <ul class="nav nav-pills nav-pills-success mb-4" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tab-p-i_1"> <i class="icon-compass pr-2"></i> Section 1</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-p-i_2"> <i class="icon-anchor pr-2"></i>Section 2</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-p-i_3"> <i class="icon-badge pr-2"></i>Section 3</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-p-i_4"> <i class="icon-badge pr-2"></i>Section 4</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-p-i_5"> <i class="icon-badge pr-2"></i>Section 5</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-p-i_6"> <i class="icon-badge pr-2"></i>Section 6</a>
          </li> -->
        </ul>

        <div class="tab-content">
          <!--Section 1-->
          <div class="tab-pane active" id="tab-p-i_1" role="tabpanel">
          <form method="post" action="/pages/sectionUpdate/{{$section->id}}" class="right-text-label-form" >
        @csrf
          <div class="form-group row {{ $errors->has('title') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Title</label>
              <div class="col-sm-5">
                <input type="text" class="form-control {{ $errors->has('title') ? 'has-input' : '' }}" name="title" id="title" placeholder="Title" value="{{ old('title', $section->title)}}">
                @if ($errors->has('title'))
                  <small class="form-text">
                      <strong>{{ $errors->first('title') }}</strong>
                  </small>
                @endif
              </div>
          </div>

          <div class="form-group row {{ $errors->has('description') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Description</label>
              <div class="col-sm-5">
                <textarea type="text" class="my-editor form-control {{ $errors->has('description') ? 'has-input' : '' }}" name="description" id="my-editor" placeholder="Description" value="{{old('description', $section->description)}}" rows="3"></textarea>
                @if ($errors->has('description'))
                  <small class="form-text">
                    <strong>{{ $errors->first('description') }}</strong>
                  </small>
                @endif
              </div>
            </div>

            <div class="form-group row {{ $errors->has('file_id') ? 'has-danger' : '' }}">
              <label for="title" class="col-sm-4 col-form-label">Select Logo</label>
                <div class="col-sm-5">  
                  <div class="input-group">
                    <span class="input-group-btn">
                    <a id="lfm" data-input="file_id" data-preview="holder2" class="btn btn-info btn-lg text-white">
                      <i class="fa fa-picture-o"></i> Choose
                    </a>
                    </span>
                    <input id="file_id" class="form-control" type="text" name="file_id">
                  </div>
                  <div id="holder2" style="margin-top:15px;max-height:100px;"></div>
                  @if ($errors->has('file_id'))
                    <small class="form-text">
                        <strong>{{ $errors->first('file_id') }}</strong>
                    </small>
                  @endif
                </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-8 ml-auto">
                <button type="submit" name="action" value="save" class="btn btn-success">Save</button>
              </div>
            </div>
        </form>
      </div>


      <!--Section 2-->
      <div class="tab-pane" id="tab-p-i_2" role="tabpanel">
      <form method="post" action="/pages/sectionCreate" class="right-text-label-form" >
      @csrf
        <div class="form-group row {{ $errors->has('title') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Title</label>
          <div class="col-sm-5">
          <input type="text" class="form-control {{ $errors->has('title') ? 'has-input' : '' }}" name="title" id="title" placeholder="Title" value="{{old('title', $section->title)}}">
          @if ($errors->has('title'))
            <small class="form-text">
              <strong>{{ $errors->first('title') }}</strong>
            </small>
          @endif
          </div>
        </div>

        <div class="form-group row {{ $errors->has('description') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Description</label>
            <div class="col-sm-5">
              <textarea type="text" class="my-editor form-control {{ $errors->has('description') ? 'has-input' : '' }}" name="description" id="my-editor" placeholder="Description" value="{{old('description', $section->description)}}" rows="3"></textarea>
              @if ($errors->has('description'))
                <small class="form-text">
                  <strong>{{ $errors->first('description') }}</strong>
                </small>
              @endif
            </div>
        </div>

        <div class="form-group row {{ $errors->has('file_id') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Select Logo</label>
          <div class="col-sm-5">  
            <div class="input-group">
              <span class="input-group-btn">
                <a id="lfm" data-input="file_id" data-preview="holder2" class="btn btn-info btn-lg text-white">
                  <i class="fa fa-picture-o"></i> Choose
                </a>
              </span>
              <input id="file_id" class="form-control" type="text" name="file_id">
            </div>
            <div id="holder2" style="margin-top:15px;max-height:100px;"></div>
            @if ($errors->has('file_id'))
                <small class="form-text">
                    <strong>{{ $errors->first('file_id') }}</strong>
                </small>
            @endif
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-8 ml-auto">
            <button type="submit" name="action" value="save" class="btn btn-success">Save2</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>



  <!--Scripts-->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script>
   var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
  </script>

  <script>
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
  </script>
  <script>
    $('#lfm').filemanager('', {prefix: route_prefix});
  </script>

  <script type="text/javascript">
    function populateslug() {
      document.getElementById('url_slug').value = document.getElementById('name').value;
    }
  </script>

  <!--CKEDITOR Script-->
<!-- <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  };
</script>
<script>
CKEDITOR.replace('my-editor', options);
</script> -->

<style>
  .popover {
    top: auto;
    left: auto;
  }
</style>
@endsection