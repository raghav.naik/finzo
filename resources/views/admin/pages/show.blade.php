@extends('admin.app')
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>  </h2>
            </div>

        </div>
    </div>
<br>
<?php 
  $page_id = basename($_SERVER['REQUEST_URI']);?>
    <div class="card card-shadow mb-4">
      <div class="card-header">
        <div class="card-title">
          {{$page_title}} Details
        </div>
      </div>
      <div class="card-body">
        @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div><br />
        @endif

        Slug : <span class="badge badge-pill badge-primary">{{$url_slug}}</span>
        <br><br>
        <ul class="nav nav-tabs mb-4" role="tablist">
          <?php $j=1; for($i=0; $i < $templates[0]->section_count; $i++) {?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tab-p-i_<?=$j?>"> Section <?=$j?></a>
          </li>
          <?php $j++; } ?>
        </ul>

        <div class="tab-content">
          
          <?php $j=1; for($i=0; $i < $templates[0]->section_count; $i++) { ?>
            <?php if($j == 1 ) { ?>
          <div class="tab-pane active" id="tab-p-i_<?=$j?>" role="tabpanel">
          <?php } else { ?>
            <div class="tab-pane" id="tab-p-i_<?=$j?>" role="tabpanel">
            <?php }?>
          <form method="post" action="/pages/sectionCreate" class="right-text-label-form" >
        @csrf

        <input type="hidden" class="form-control {{ $errors->has('id') ? 'has-input' : '' }}" name="id" id="id" placeholder="id" value="35">

        <input type="hidden" class="form-control {{ $errors->has('page_id') ? 'has-input' : '' }}" name="page_id" id="page_id" placeholder="page_id" value="{{$id}}">

          <div class="form-group row {{ $errors->has('title') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Title</label>
              <div class="col-sm-5">
                <input type="text" class="form-control {{ $errors->has('title') ? 'has-input' : '' }}" name="title" id="title" placeholder="Title" value="">
                @if ($errors->has('title'))
                  <small class="form-text">
                      <strong>{{ $errors->first('title') }}</strong>
                  </small>
                @endif
              </div>
          </div>

          <div class="form-group row {{ $errors->has('description') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Description</label>
              <div class="col-sm-5">
                <textarea type="text" class="my-editor form-control {{ $errors->has('description') ? 'has-input' : '' }}" name="description" id="my-editor" placeholder="Description" value="" rows="3"></textarea>
                @if ($errors->has('description'))
                  <small class="form-text">
                    <strong>{{ $errors->first('description') }}</strong>
                  </small>
                @endif
              </div>
            </div>

            <div class="form-group row {{ $errors->has('file_id') ? 'has-danger' : '' }}">
              <label for="title" class="col-sm-4 col-form-label">Select Logo</label>
                <div class="col-sm-5">  
                  <div class="input-group">
                    <span class="input-group-btn">
                    <a id="lfm" data-input="file_id" data-preview="holder2" class="btn btn-info btn-lg text-white">
                      <i class="fa fa-picture-o"></i> Choose
                    </a>
                    </span>
                    <input id="file_id" class="form-control" type="text" name="file_id">
                  </div>
                  <div id="holder2" style="margin-top:15px;max-height:100px;"></div>
                  @if ($errors->has('file_id'))
                    <small class="form-text">
                        <strong>{{ $errors->first('file_id') }}</strong>
                    </small>
                  @endif
                </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-8 ml-auto">
                <button type="submit" name="action" value="save" class="btn btn-success">Save<?=$j?></button>
              </div>
            </div>
        </form>
      </div>
      <?php $j++; } ?>
    </div>
  </div>
</div>



  <!--Scripts-->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script>
   var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
  </script>

  <script>
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
  </script>
  <script>
    $('#lfm').filemanager('', {prefix: route_prefix});
  </script>

  <script type="text/javascript">
    function populateslug() {
      document.getElementById('url_slug').value = document.getElementById('name').value;
    }
  </script>

  <!--CKEDITOR Script-->
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  };
</script>
<script>
CKEDITOR.replace('my-editor', options);
</script>

<style>
  .popover {
    top: auto;
    left: auto;
  }
</style>
@endsection