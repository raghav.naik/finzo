@extends('admin.app')
@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Add Pages
                </div>
            </div>
  <div class="card-body">    
      <form method="post" action="{{ route('pages.store') }}" class="right-text-label-form"  enctype="multipart/form-data">
        @csrf

        <div class="form-group row {{ $errors->has('parent_id') ? 'has-danger' : '' }}">
          <label for="parent_id" class="col-sm-4 col-form-label">Parent</label>
          <div class="col-sm-5">
            <select class="form-control {{ $errors->has('parent_id') ? 'has-input' : '' }}" name="parent_id" id="parent_id" >
              <option id="parent_id" value="0">None</option>
              @foreach($pages_details as $value)
              <option id="parent_id" value="{{$value->id}}">{{$value->title}}</option>
              @endforeach
            </select>
            @if ($errors->has('parent_id'))
            <small class="form-text">
              <strong>{{ $errors->first('parent_id') }}</strong>
            </small>
            @endif
          </div>
        </div>  

        <div class="form-group row {{ $errors->has('title') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Title</label>
          <div class="col-sm-5">
              <input type="text" class="form-control {{ $errors->has('title') ? 'has-input' : '' }}" name="title" id="title" placeholder="Title" value="{{old('title')}}">
              @if ($errors->has('title'))
                  <small class="form-text">
                      <strong>{{ $errors->first('title') }}</strong>
                  </small>
              @endif
          </div>
        </div>

        <div class="form-group row {{ $errors->has('name') ? 'has-danger' : '' }}">
          <label for="name" class="col-sm-4 col-form-label">Name</label>
          <div class="col-sm-5">
              <input type="text" class="form-control {{ $errors->has('name') ? 'has-input' : '' }}" name="name" id="name" placeholder="Name" value="{{old('name')}}" onkeyup="populateslug();">
              @if ($errors->has('name'))
                  <small class="form-text">
                      <strong>{{ $errors->first('name') }}</strong>
                  </small>
              @endif
          </div>
        </div>
       
        <div class="form-group row {{ $errors->has('url_slug') ? 'has-danger' : '' }}">
          <label for="url_slug" class="col-sm-4 col-form-label">Slug</label>
          <div class="col-sm-5">
              <input type="text" class="form-control {{ $errors->has('url_slug') ? 'has-input' : '' }}" name="url_slug" id="url_slug" placeholder="url_slug" value="{{old('url_slug')}}" readonly="">
              @if ($errors->has('url_slug'))
                  <small class="form-text">
                      <strong>{{ $errors->first('url_slug') }}</strong>
                  </small>
              @endif
          </div>
        </div>

        <div class="form-group row {{ $errors->has('description') ? 'has-danger' : '' }}">
            <label for="title" class="col-sm-4 col-form-label">Description</label>
            <div class="col-sm-5">
                <textarea type="text" class="form-control summernote }}" name="description" id="my-editor" placeholder="Description" value="{{old('description')}}"></textarea>
                @if ($errors->has('description'))
                    <small class="form-text">
                        <strong>{{ $errors->first('description') }}</strong>
                    </small>
                @endif
            </div>
        </div>


        <div class="form-group row {{ $errors->has('file_name') ? 'has-danger' : '' }}">
          <label for="title" class="col-sm-4 col-form-label">Select Image</label>
          <div class="col-sm-5">  
            <div class="input-group">
              <span class="input-group-btn">
                <a id="lfm" data-input="file_name" data-preview="holder2" class="btn btn-info btn-lg text-white">
                  <i class="fa fa-picture-o"></i> Choose
                </a>
              </span>
            <input id="file_name" class="form-control" type="text" name="file_name">
            </div>
            <div id="holder2" style="margin-top:15px;max-height:100px;"></div>
              @if ($errors->has('file_name'))
                  <small class="form-text">
                      <strong>{{ $errors->first('file_name') }}</strong>
                  </small>
              @endif
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-8 ml-auto">
              <button type="submit" name="action" value="save" class="btn btn-success">Save</button>
              <button type="submit" name="action" value="publish" class="btn btn-success">Save & Publish</button>
          </div>
        </div>
      </form>
  </div>
</div>
</div>
</div>

  <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script>
   var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
  </script>

  <script>
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
  </script>
  <script>
    $('#lfm').filemanager('', {prefix: route_prefix});
  </script>


  <script type="text/javascript">
    function populateslug() {
      document.getElementById('url_slug').value = document.getElementById('name').value;
      var lower = $('input#name').val().toLowerCase();
        var hyp = lower.replace(/ /g,"-");
        $('input#url_slug').val(hyp);
    }
  </script>


<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  };
</script>

<style>
  .popover {
    top: auto;
    left: auto;
  }
</style>
@endsection