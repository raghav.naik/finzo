@extends('admin.app')
@section('content')
<div class="row">
    <div class=" col-md-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    Add Role
                </div>
            </div>
            <div class="card-body">
                <form method="POST" class="right-text-label-form" action="/admin/roles">
                    @csrf
                    <div class="form-group row {{ $errors->has('title') ? 'has-danger' : '' }}">
                        <label for="title" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control {{ $errors->has('title') ? 'has-input' : '' }}" name="title" id="title" placeholder="Title" value="{{old('title')}}">
                            @if ($errors->has('title'))
                                <small class="form-text">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('name') ? 'has-danger' : '' }}">
                        <label for="name" class="col-sm-4 col-form-label">Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control {{ $errors->has('name') ? 'has-input' : '' }}" name="name" id="name" placeholder="Name" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                <small class="form-text">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('status') ? 'has-danger' : '' }}">
                        <label for="status" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-5">
                            <select class="form-control {{ $errors->has('status') ? 'has-input' : '' }}" name="status" id="status" >
                                <option value="1">Active</option>
                                <option value="0">Disabled</option>
                            </select>
                            @if ($errors->has('status'))
                                <small class="form-text">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 ml-auto">
                            <button type="submit" class="btn btn-success">
                                Create Role
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
