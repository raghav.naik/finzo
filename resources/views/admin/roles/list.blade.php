@extends('admin.app')
@section('content')
    @if(session()->has('alert-success'))
        <div class="alert alert-success">
            {{ session()->get('alert-success') }}
        </div>
    @endif
<div class="row">
    <div class=" col-xl-12">
        <div class="card card-shadow mb-4">
            <div class="card-header">
                <div class="card-title">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            Role List
                        </div>
                        @can('roles.create')
                        <div class="col-sm-12 col-md-6 text-right">
                            <a href="/admin/roles/create" class="btn btn-info">Add Role</a>
                        </div>
                        @endrole
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Name</th>
                            <th scope="col">Status</th>
                            @if(Auth::user()->hasAnyPermission(['roles.edit']))
                            <th scope="col">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $role)
                        <tr>
                            <th scope="row">{{$role->id}}</th>
                            <td>{{$role->title}}</td>
                            <td>{{$role->name}}</td>
                            <td>{{$role->status == '1' ? 'Active' : 'In Active'}}</td>
                            <td>
                            @can('roles.edit')
                                <a href="/admin/roles/{{$role->id}}/edit" class="btn btn-dark btn-sm"><i class="icon-note "></i></a>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                        @if(count($roles) == 0)
                        <tr>
                            <td colspan="5">
                            {{"No Roles Found"}}
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                 {{ $roles->onEachSide(1)->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
