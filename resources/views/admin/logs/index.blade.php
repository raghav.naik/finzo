@extends('admin.app')
@section('content')

<div class="row">
   	<div class=" col-xl-12">
      	<div class="card card-shadow mb-4">
        	<div class="card-header">
				<div class="card-title">
					<div class="row">
						<div class="col-sm-12 col-md-6">Logs</div>			  	
					</div>
				</div>
			</div>
    
			<div class="card-body">
					@if(session()->get('success'))
					<div class="alert alert-success">
					{{ session()->get('success') }}  
					</div><br />
				@endif
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#ID</th>
							<th scope="col">User</th>
							<th scope="col">Description</th>
							<th scope="col">Module</th>
							<th scope="col">Url</th>
							<th scope="col">Method</th>
						</tr>
					</thead>
					<tbody>
						@foreach($logs as $log)
						<tr>
							<td>{{$log->id}}</td>								
							<td>{{$log->first_name. ' ' . $log->last_name}}</td>
							<td>{{$log->description}}</td>
							<td>{{$log->model}}</td>
							<td>{{$log->url}}</td>
							<td>{{$log->method}}</td>							
						</tr>
							@endforeach
					@if(count($logs) == 0)
					<tr>
						<td colspan="3">
						{{"No Logs Found"}}
						</td>
					</tr>
					@endif
					</tbody>	
				</table>
				{{ $logs->onEachSide(1)->links() }}
			</div>
		</div>
	</div>
</div>
@endsection