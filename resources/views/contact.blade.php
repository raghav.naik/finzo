 @extends('layouts.master')
 @section('pageTitle', 'Contact')
 @section('content')
<!-- Main Section -->
<div class="page-wrapper opr-current">
    <section class="cover devider-section">
        <div class="cover__bg"> <img src="{{ URL::asset('images/contact-banner.jpg') }}" alt=""> </div>
        <div class="cover__overlay main__cover animatable">
            <h1>Contact</h1></div>
        <div class="bottom__nav section-scroll">
            @include('includes.contactBottomNav') 
        </div>
    </section>
    <div class="devider-section" id="getInTouch" data-section=".getInTouch">
        <nav class="breadcrumbs animatable">
         {{ Breadcrumbs::render('contact') }}
        </nav>
        <div class="head-styled animatable p-0">
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-6">
                <h4 class="custom-heading">Get in Touch</h4>
              </div>
              <div class="col-12 col-md-6 pl-custom mb-4-sm">
                <h2>We’d love to hear from you, get in touch</h2>
              </div>
            </div>
            <section class="head-styled-section vision">
                <div class="container contact-section">
                <div class="parallax-layer" id="parallaxSecond"> <img data-depth="0.80" src="{{ URL::asset('images/global-map.png') }}" alt=""> </div>
                    <div class="row">
                    <div class="col-md-6 d-flex revealer">
                        <div class="revealer__image animatable"> <img src="{{ URL::asset('images/contact/contact_main.jpg') }}" alt=""> </div>
                        <!-- <img src="images/contact/contact_main.jpg" class="contact-img-left"> --></div>
                        <div class="col-md-6 d-flex vertical-align-middle p-70">
                        <form class="custom-form" method='POST' action="{{ url('insert') }}" id="contact_form">
                            <fieldset>
                            <div class="row form-elements animatable"> {{ csrf_field() }}
                                <div class="col-12"> @if(session()->get('success'))
                                    <div class="alert alert-success"> {{ session()->get('success') }} </div>
                                    <br /> @endif
                                    <div class="input-wrapper {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <input placeholder="Name" value="{{ old('name') }}" class="cf-input" type="text" name="name" /> @if ($errors->has('name')) <span class="help-block">                             <strong>{{ $errors->first('name') }}</strong>                           </span> @endif </div>
                                </div>
                                <div class="col-12">
                                    <div class="input-wrapper {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input placeholder="Email" value="{{ old('email') }}" class="cf-input" type="text" name="email" /> @if ($errors->has('email')) <span class="help-block">                             <strong>{{ $errors->first('email') }}</strong>                           </span> @endif </div>
                                </div>
                                <div class="col-12">
                                    <div class="input-wrapper {{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <input placeholder="Phone" class="cf-input" type="text" name="phone" value="{{ old('phone') }}" /> @if ($errors->has('phone')) <span class="help-block">                             <strong>{{ $errors->first('phone') }}</strong>                           </span> @endif </div>
                                </div>
                                <div class="col-12">
                                    <div class="input-wrapper">
                                        <textarea placeholder="Message Here" class="cf-input" value="{{ old('message') }}" name="message" cols="5" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-12" id="contact-sec">
                                    <button type="submit" class="btn-submit-custom pl-3 pr-3 pt-2 pb-2">Submit</button>
                                </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            </div>
        <div class="devider-section" id="contactUs" data-section=".contactUs">
            <section class="address-map">
                <div class="container mt-18">
                    <div class="row">
                        <div class="col-md-12 col-12 align-items-end contact-details-container">
                            <div class="contact-details">
                                <h4 class="pb-3 text-capitalize" style="margin-bottom: 18px !important;">Contact Us </h4>
                                <h4 class="primary-heading text-capitalize">Phone: <span>+973 17 290333</span></h4>
                                <h4 class="primary-heading text-capitalize">Fax: <span>+973 17 290050</span></h4>
                                <h4 class="primary-heading text-capitalize">Email Address: <span class="text-lowercase">marketing@afs.com.bh</span></h4>
                                <h4 class="text-capitalize pt-4 pb-3" style="margin-bottom: 18px !important;">For Support, Please Contact</h4>
                                <h4 class="primary-heading text-capitalize">Address:</h4>
                                <h4>P.O.Box 2152, Manama, Kingdom of Bahrain</h4> </div>
                            <div class="custom-map" style="display: none;"> <img src="{{ URL::asset('images/contact/contact_map.jpg') }}"> </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</div>
<footer class="devider-section">
    <div class="row opr-common justify-content-between">
        <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#"> <span>Fintech Innovation in a Box</span> <i class="circle__move"></i> </a>
        </div>
        <div class="buttons animatable">
            <div class="devider_button">
                <a class="button button_with-icon p-4"> <span></span> </a>
            </div>
            <div class="text-right bottom__footer">
                <buttton class="button button_with-icon button_normal top-btn"> <span>Top</span> <i class="circle__move"></i> </buttton>
                <p>Finzo Co. B.S.C. (c) All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
</div>
 @stop
 <script>
    var secondScene = document.getElementById('parallaxSecond');
    var secondParallax = new Parallax(secondScene);
    if (window.matchMedia("(min-width: 768px)").matches) {
        $(function() {
            $.scrollify({
                interstitialSection: ".devider-section",
                offset: 0,
                before: function() {
                    var currentSection = $.scrollify.current().data('section');
                    $('.sticky--nav').find('li').removeClass('active');
                    $(currentSection).addClass('active');
                }
            });
        });
    }
</script>