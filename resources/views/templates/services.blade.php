@extends('layouts.master')
@section('pageTitle', 'Services')
@section('content')
@section('contents')
	<div class="header__menu_inner">
		<a class="header__logo">
		<img src="{{ URL::asset('images/logo.png') }}" alt></a>
	</div>
  @endsection
 <!-- Main Section -->
      <!-- Main Section -->
      <div class="page-wrapper opr-current">
			<section class="cover devider-section-interstial">
				@if($pageData->parent)
					<div class="cover__bg">
						<img src="{{$pageData->parent->files_path}}" alt="">
					</div>
					<div class="cover__overlay main__cover animatable">
						<h1>{{$pageData->parent->name}}</h1>
					</div>
			  	@endif			  
			</section>
			<nav class="breadcrumbs animatable">
			  <a href="index.html" title="Home">Home</a>
			  <a href="services.html" title="Services">Services</a>
			</nav>
			<section class="services-custom service--block p-3-new">
			  <div class="container">
				@foreach ($pageData->child as $item)
					<div class="devider-section">
						@if ($item->title)
						<div class="row">
						  <div class="col-md-2"></div>
						  <div class="col-md-3">
						  <h4 class="custom-heading">{{$item->title}}</h4>
						  </div>
						</div>
						@endif
						<div class="row">
						  <div class="col-md-2"></div>
						  <div class="col-md-3 mt-7-solutions pl-0">
							<img src="{{$item->file_path}}" class="services-icon1">
						  </div>
						</div>
						<div class="row">
						  <div class="col-md-2"></div>
						  <div class="col-md-9 mt-5 border-after pl-0">
							<h2>{{$item->name}}</h2>
						  </div>
						</div>
						<div class="row mb-7-solutions">
						  <div class="col-md-2"></div>
						  <div class="col-md-9 custom-list-items">							
							{!! $item->description !!}
						  </div>
						</div>
					</div>
				@endforeach
			  </div>
			</section>
		  </div>
		  <footer class="devider-section-interstial">
			<div class="row opr-common justify-content-between">
			  <div class="buttons finetech-btn animatable">
				<a class="button button_with-icon button_normal" href="#">
				  <span>Fintech Innovation in a Box</span>
				  <i class="circle__move"></i>
				</a>
			  </div>
			  <div class="buttons animatable">
				<div class="devider_button">
				  <a class="button button_with-icon" href="contact.html">
					<span>Contact Us</span>
					<i class="icon-play"></i>
				  </a>
				</div>
				<div class="text-right bottom__footer">
				  <buttton class="button button_with-icon button_normal top-btn">
					<span>Top</span>
					<i class="circle__move"></i>
				  </buttton>
				  <p>Finzo Co. B.S.C. (c) All Rights Reserved</p>
				</div>
			  </div>
			</div>
		  </footer>
    </div>
 @stop