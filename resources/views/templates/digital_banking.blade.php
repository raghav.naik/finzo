@extends('layouts.master')
@section('pageTitle', 'Digital Banking')
@section('content') 
 <!-- Main Section -->
      <div class="page-wrapper opr-current">
        @include('includes.solutionsBottomNav')
        <div class="cover__slider">
          <div class="opr-page">
            <nav class="breadcrumbs animatable">
             {{ Breadcrumbs::render('digital_banking') }}
            </nav>
           <section class="team-grid service--block devider-section">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-md-3"></div>
                  <div class="col-12 col-md-9">
                    <div class="team-grid-header team-grid-header--services animatable">
                      <h4>Digital Banking</h4>
                      <h2>Mobile first banking with feature rich stack & personalized end user experience</h2>
                    </div>
                    <div class="team-grid-thumb">
                      <div class="revealer has-styled-border">
                        <div class="revealer__image animatable">
                          <img src="{{ URL::asset('images/DIGITAL_BANKING_INSIDE_SECOND_BLOCK.jpg')}}" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="service_two_half service_two_half--ltr devider-section">
              <div class="container">
                <div class="row revealer">
                  <div class="col-12 col-md-6 has-styled-border">
                    <div class="revealer__image animatable">
                      <img src="{{ URL::asset('images/digital-banking-2.jpg') }}" alt="">
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="revealer__text">
                      <ul class="animatable">
                        <li>
                          <p>Fully functional sandbox environment to foster collaboration & innovation</p>
                        </li>
                        <li>
                          <p>Open Banking/API based platform offering scalable & agile delivery</p>
                        </li>
                        <li>
                          <p>Modularized composable architecture for agile delivery of modern banking capabilities and to foster innovation</p>
                        </li>
                        <li>
                          <p>Data driven architecture with 360 degree customer insights & predictive analytics</p>
                        </li>
                        <li>
                          <p>Best in class customer journeys across Personal, SME and Large Enterprise Banking Segments</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="service_two_half service__bullets devider-section">
              <div class="container">
                <h4>Core Banking Digitized</h4>
                <div class="revealer__text">
                  <ul class="animatable">
                    <li><p>Biometrics</p></li>
                    <li><p>Virtual Cards</p></li>
                    <li><p>Loyalty</p></li>
                    <li><p>Micro Lending</p></li>
                    <li><p>eKYC</p></li>
                    <li><p>EComm Market Place</p></li>
                    <li><p>Instant Creation of Current/Saving Account</p></li>
                    <li><p>Merchant Payments</p></li>
                    <li><p>eLoan application & Approvals</p></li>
                    <li><p>Bill Payment</p></li>
                    <li><p>International Remittance</p></li>
                    <li><p>AI Based ChatBot</p></li>
                    <li><p>International Telco Top-up</p></li>
                    <li><p>Performance Management</p></li>
                    <li><p>Omni Channel User Experience</p></li>
                    <li><p>Virtual Cards</p></li>
                  </ul>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <footer class="devider-section">
        <div class="row opr-common justify-content-between">
          <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#">
              <span>Fintech Innovation in a Box</span>
              <i class="circle__move"></i>
            </a>
          </div>
          <div class="buttons animatable">
            <!-- <div class="devider_button">
              <a class="button button_with-icon" href="finzo-pay.html">
                <span>FinzoPay</span>
                <i class="icon-play"></i>
              </a>
            </div> -->
            <div class="text-right bottom__footer">
              <buttton class="button button_with-icon button_normal top-btn">
                <span>Top</span>
                <i class="circle__move"></i>
              </buttton>
              <p>Finzo Co. B.S.C. (c) All Rights Reserved</p>
            </div>
          </div>
        </div>
      </footer>
     </div>
    <!-- Portrait Mode Only -->
    <div class="portrait__mode"><p>Please Use Portrait Mode</p></div>
      @stop
    <script src="{{ URL::asset('js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ URL::asset('js/common.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.scrollify.js') }}"></script>
    <script type="text/javascript">
       if (window.matchMedia("(min-width: 768px)").matches){
        $(function() {
          $.scrollify({
            interstitialSection : ".devider-section",
            offset : -100,
          });
        });
      }
    </script>
  </body>
</html>