@extends('layouts.master')
@section('pageTitle','Finzo Pay')
@section('content')
 <!-- Main Section -->
      <div class="page-wrapper opr-current">
          @include('includes.solutionsBottomNav')
        <div class="cover__slider">
          <div class="opr-page">
            <nav class="breadcrumbs animatable">
               {{ Breadcrumbs::render('finzo_pay') }}
            </nav>
             <section class="team-grid devider-section">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-md-3"></div>
                  <div class="col-12 col-md-9">
                    <div class="team-grid-header team-grid-header--services animatable">
                      <h4>FinzoPay</h4>
                      <h2>Interoperable wallet ecosystem to allow acceptance of transactions across wallets world over</h2>
                    </div>
                    <div class="team-grid-thumb">
                      <div class="revealer has-styled-border">
                        <div class="revealer__image animatable">
                          <img src="{{ URL::asset('images/FINZOPAY_INSIDEPAGE_BLOCK1.jpg') }}" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="service_two_half service_two_half--ltr devider-section">
              <div class="container">
                <div class="row revealer">
                  <div class="col-12 col-md-6 has-styled-border">
                    <div class="revealer__image animatable">
                      <img src="{{ URL::asset('images/FINZOPAY_INSIDEPAGE_BLOCK2.jpg') }}" alt="">
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="revealer__text">
                      <ul class="animatable">
                        <li>
                          <h5>Opportunity for Banks, Telcos, Corporates & others</h5>
                          <p>Use the robust modularized ecosystem to launch a wallet solution for your business group
                          </p>
                        </li>
                        <li>
                          <h5>Multi-currency wallet support: </h5>
                          <p>Make transactions globally with option to pay in local currency</p>
                        </li>
                        <li>
                          <h5>End-to-end secure cloud based resilient payment infrastructure: </h5>
                          <p>Secure transaction platform capable of handling volume and best in class uptime</p>
                        </li>
                        <li>
                          <h5>Opportunity to be part of a growing wallet network </h5>
                          <p>Join the growing network of wallet payments across the globe</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <footer class="devider-section">
        <div class="row opr-common justify-content-between">
          <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#">
              <span>Fintech Innovation in a Box</span>
              <i class="circle__move"></i>
            </a>
          </div>
          <div class="buttons animatable">
            <div class="devider_button">
              <a class="button button_with-icon" href="{{url('/home/solutions/digital-banking')}}">
                <span>Digital Banking</span>
                <i class="icon-play"></i>
              </a>
            </div>
            <div class="text-right bottom__footer">
              <buttton class="button button_with-icon button_normal top-btn">
                <span>Top</span>
                <i class="circle__move"></i>
              </buttton>
              <p>Finzo Co. B.S.C. (c) All Rights Reserved</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    @stop