@extends('layouts.master')
@section('pageTitle', 'Digital Wallet')
@section('content')
 <!-- Main Section -->
      <div class="page-wrapper opr-current">
        @include('includes.solutionsBottomNav')
        <div class="cover__slider">
          <div class="opr-page">
            <nav class="breadcrumbs animatable">
             {{ Breadcrumbs::render('digital_wallet') }}
            </nav>
            <section class="team-grid devider-section">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-md-3"></div>
                  <div class="col-12 col-md-9">
                    <div class="team-grid-header team-grid-header--services animatable">
                      <h4>Digital Wallet</h4>
                      <h2>Agile scalable feature rich wallet platform with host of features to amplify user experience and utility</h2>
                    </div>
                    <div class="team-grid-thumb">
                      <div class="revealer has-styled-border">
                        <div class="revealer__image animatable">
                          <img src="{{ URL::asset('images/FINZO_DIGITALWALLET.jpg') }}" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="service_two_half service_two_half--ltr devider-section">
              <div class="container">
                <div class="row revealer">
                  <div class="col-12 col-md-6 has-styled-border">
                    <div class="revealer__image animatable">
                      <img src="{{ URL::asset('images/DIGITALWALLET_INSIDEPAGE_BLOCK2.jpg') }}" alt="">
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="revealer__text">
                      <ul class="animatable">
                        <li>
                          <h5>Frictionless eKYC & Onboarding</h5>
                          <p>Electronic KYC process allowing easy signup and onboarding.</p>
                        </li>
                        <li>
                          <h5>Multiple top up options</h5>
                          <p>Debit/Credit Card, Card on File, Net Banking or Cash Top Ups to deliver seamless experience.</p>
                        </li>
                        <li>
                          <h5>Peer to Peer Transfers</h5>
                          <p>Frictionless instant transfers to friends & family with a few clicks on the mobile.</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="service_two_half service_two_half--rtl devider-section">
              <div class="container">
                <div class="row revealer">
                  <div class="col-12 col-md-6 has-styled-border">
                    <div class="revealer__image animatable">
                      <img src="{{ URL::asset('images/DIGITALWALLET_INSIDEPAGE_BLOCK3.jpg') }}" alt="">
                    </div>
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="revealer__text">
                      <ul class="animatable">
                        <li>
                          <h5>Peer to Merchant Transfers</h5>
                          <p>Make payments to participating merchants in the network across the globe</p>
                        </li>
                        <li>
                          <h5>Innovative Payments</h5>
                          <p>Delivering NFC, QR Code (Dynamic / Static), Wearable, Biometric based payment solutions</p>
                        </li>
                        <li>
                          <h5>Integration with 3rd party Websites, Mobile Web or Apps</h5>
                          <p>API based integration with partner websites or Apps to scale up business and grow digital footprint</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="bwallet devider-section">
              <figure class="inner--cover" data-src="{{ URL::asset('images/FINZO_DIGITALWALLET_BWALLET.jpg') }}"></figure>
              <div class="container container--lg">
                <div class="row bwallet__align-top">
                  <div>
                    <div class="inner-heading">
                      <h4 style="text-transform: lowercase;">BWallet</h4>
                    </div>
                  </div>
                  <div class="inner-main_content">
                    <div class="row service_two_half">
                      <div class="col-12 col-md-6 p-0">
                        <div class="revealer__text inner-sub_content-l">
                          <ul class="animatable">
                            <li>
                              <p>One of the fastest growing mobile wallet in the Region trusted by over 100,000 people</p>
                            </li>
                            <li>
                              <p>Launched by AFS in partnership with Batelco in January 2018</p>
                            </li>
                            <li>
                              <p>Available for Bahraini & Resident Expats</p>
                            </li>
                            <li>
                              <p>Available to customers of all telecom operators</p>
                            </li>
                            <li>
                              <p>Enables Peer-to-Peer (P2P) and Peer-to-Merchant (P2M) payments</p>
                            </li>
                            <li>
                              <p>First in Bahrain to deliver P2P instant money transfer</p>
                            </li>
                            <li>
                              <p>Ground breaking eKYC (Know Your Customer) process to accelerate customer registration</p>
                            </li>
                            <li>
                              <p>Strong merchant ecosystem: Over 3,000 QR POS devices accepting payments across Bahrain</p>
                              <p>Over 100 Offers, Discounts across merchants driving adoption & growth</p>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-12 col-md-6 p-0">
                        <div class="revealer__text inner-sub_content-r">
                          <ul class="animatable">
                            <li>
                              <div class="count-tagline">Trusted By</div>
                              <div class="row">
                                <div class="counter-amount">100,000</div>
                                <div class="counter-text">People</div>
                              </div>
                            </li>
                            <li>
                              <div class="count-tagline">Over</div>
                              <div class="row">
                                <div class="counter-amount">3,000</div>
                                <div class="counter-text">QR POS devices</div>
                              </div>
                            </li>
                            <li>
                              <div class="count-tagline">Over</div>
                              <div class="row">
                                <div class="counter-amount">100</div>
                                <div class="counter-text">Offers discounts across merchants driving adoption & growth</div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <footer class="devider-section">
        <div class="row opr-common justify-content-between">
          <div class="buttons finetech-btn animatable">
            <a class="button button_with-icon button_normal" href="#">
              <span>Fintech Innovation in a Box</span>
              <i class="circle__move"></i>
            </a>
          </div>
          <div class="buttons animatable">
            <div class="devider_button">
              <a class="button button_with-icon" href="{{url('/home/solutions/finzo-pay')}}">
                <span>FinzoPay</span>
                <i class="icon-play"></i>
              </a>
            </div>
            <div class="text-right bottom__footer">
              <buttton class="button button_with-icon button_normal top-btn">
                <span>Top</span>
                <i class="circle__move"></i>
              </buttton>
              <p>Finzo Co. B.S.C. (c) All Rights Reserved</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
      @stop