<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use Finzo\Files;

class Offers extends Model
{
  protected $fillable = [
    'title',
    'file_id',
    'description',
    'priority',
    'expiry_date'
  ];

  public function file()
{
    return $this->belongsTo(Files::class);
}

}