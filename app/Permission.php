<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
		'name',
		'title'		
	];
	//protected $table = 'Permission'; 

	public static function checkValidPermission($permissionName) {
		$permission = Permission::where('name', '=', $permissionName)->first();
		if ($permission === null) {
			return false;
		}
		return true;
	}

}
