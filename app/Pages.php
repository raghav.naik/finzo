<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use Finzo\Files;
use DB;

class Pages extends Model
{
	protected $table = 'cms_pages';

  protected $fillable = [
    'title',
    'name',
    'description',
    'url_slug',
    'parent_id',
    'publish',
    'template_id'
  ];

    public function file()
    {
        return $this->belongsTo(Files::class);
    }

    public static function PagesParent()
    {
        $parent = DB::table('cms_pages')
                    ->whereNotNull('cms_pages.title')
                    ->get();
        return $parent;
    }

    public static function PagesDetails($id)
    {
        $template = DB::table('cms_pages')
                    ->join('files', 'cms_pages.id', '=', 'files.asset_id')
                    ->select('*')
                    ->where('cms_pages.id', '=', $id)
                    ->get();
        return $template;
    }

    public static function FileDetails($id)
    {
        $file_Details = DB::table('files')
                    ->join('cms_pages', 'files.asset_id', '=', 'cms_pages.id')
                    ->select('*', 'files.id')
                    ->where('files.asset_id', '=', $id)
                    ->get();
        return $file_Details;
    }

}