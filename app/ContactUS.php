<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;

class ContactUS extends Model
{
	public $table = 'contact_us';


	protected $fillable = [
		'name',
		'email',
		'phone',
		'message'
	];
}
