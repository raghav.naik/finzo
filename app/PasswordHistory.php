<?php

namespace Finzo;


use Illuminate\Database\Eloquent\Model;

class PasswordHistory extends Model
{
    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo('Finzo\User');
    }
}
