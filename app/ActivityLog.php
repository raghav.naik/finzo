<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use DB;

class ActivityLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'model', 'url', 'method', 'ip', 'agent', 'user_id'
    ];

    public static function getActivityLog() 
    {
        $log = DB::table('activity_logs')                
                ->join('users', 'users.id', '=', 'activity_logs.user_id') 
                ->orderBy('activity_logs.id', 'DESC')
                ->paginate(10);                
        return $log;

    }
}
