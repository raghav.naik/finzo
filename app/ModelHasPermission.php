<?php

namespace Finzo;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModelHasPermission extends Model
{
    
     /**
    * delete the model_id from ModelHasPermission
    *
    * @param  int  $id
    */
    public function destroyPermission($id){

    	DB::table('model_has_permissions')->where('model_id',$id)->delete();
    	return 1;
	}

}
