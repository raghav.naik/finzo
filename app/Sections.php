<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use Finzo\Files;
use Finzo\Pages;
use DB;

class Sections extends Model
{
	protected $table = 'sections';

  protected $fillable = [
    'title',
    'description',
    'page_id'
  ];

    public function file()
{
    return $this->belongsTo(Files::class);
}

public function scopeDetails($cmsPage)
{
	$url = basename($_SERVER['REQUEST_URI']);
  	$cmsPage = DB::table('cms_pages')
  					->where('id', $url)
  					->select('id' ,'title', 'url_slug', 'template_id')->first();
	return $cmsPage;
}

public function scopeTemplateDetails()
{

	$url = basename($_SERVER['REQUEST_URI']);
	$template = DB::table('cms_pages')
				->join('page_template', 'cms_pages.template_id', '=', 'page_template.id')
				->select('page_template.id', 'cms_pages.template_id', 'page_template.section_count')
				->where('cms_pages.id', '=', $url)
				->get();
  	return $template;
}

public function scopeSectiondetails()
{

	$url = basename($_SERVER['REQUEST_URI']);
  	$check = DB::table('cms_pages')
  					->where('id', $url)->select('id')->first();
  	$sectionValues = DB::table('sections')
  					->where('page_id', $check->id)->get();
  	return $sectionValues;
}

}