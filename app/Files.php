<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use Finzo\Offers;
use Finzo\Pages;
use Finzo\Sections;

class Files extends Model
{
    protected $fillable = [
    'file_name',
    'file_path',
    'type',
    'asset_id'
    
  ];

public function offer()
{
    return $this->hasMany(Offers::class);
}

public function page()
{
    return $this->hasMany(Pages::class);
}

public function section()
{
    return $this->hasMany(Sections::class);
}

}