<?php

namespace Finzo;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PasswordHistory;
use PasswordSecurity;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password', 'username','last_name','phone','gender','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Password Histories
    *
    */
    public function passwordHistories()
    {
        return $this->hasMany('Finzo\PasswordHistory');
    }

    /**
    * Password Security
    *
    */
    public function passwordSecurity()
    {
        return $this->hasOne('Finzo\PasswordSecurity');
    }
}
