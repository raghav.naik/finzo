<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Cms extends Model
{
    public function listStaticPages($urlSlug = null, $id = null, $parentId = null, $countFlag = 0 )
    {
        $data = DB::table('cms_pages');
                if($countFlag)
                    $data->select(DB::raw('count(*) as count'));
                else
                    $data->select('cms_pages.id', 'cms_pages.title', 'cms_pages.name', 'cms_pages.description', 'cms_pages.parent_id', 'files.id as file_id', 'files.file_path');
                    $data->leftJoin('files', 'files.asset_id', 'cms_pages.id');
                if($urlSlug)
                    $data->where('cms_pages.url_slug','=',$urlSlug);
                if($id)
                    $data->where('cms_pages.id','=',$id);
                if($parentId)
                    $data->where('cms_pages.parent_id','=',$parentId);

                $data->where('cms_pages.publish','=',1)
		          ->orderBy('cms_pages.id','asc');
		         
	   return $data->get(); 
    }
}
