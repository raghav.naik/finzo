<?php

namespace Finzo;

use Illuminate\Database\Eloquent\Model;
use Authenticatable, Authorizable, CanResetPassword, HasRoles;
use DB;

class ModelHasRoles extends Model
{ 
    // to resolve the error while entering the data to the database as created_at and updated_at fields are not persent in ModelHasRoles 
    public $timestamps = false;
    /**
    * fetch the role of user
    *
    * @param  int  $id
    * @return array
    */
    public function userRole($id)
    {
        return DB::table('model_has_roles')->where('model_id',$id)->get() ;
        
    }
}


   
