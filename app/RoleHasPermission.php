<?php

namespace Finzo;

use DB;
use Illuminate\Database\Eloquent\Model;

class RoleHasPermission extends Model
{
	// to resolve the error while entering the data to the database as created_at and updated_at fields are not persent in ModelHasRoles 
  	public $timestamps = false;

     /**
    * delete the role_id from roleHasPermission
    *
    * @param  int  $id
    */

    public function destroyPermission($id){
    	DB::table('role_has_permissions')->where('role_id',$id)->delete();
	}
}
