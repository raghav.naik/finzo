<?php
namespace Finzo\Helpers;
use Request;
use Finzo\ActivityLog as LogActivityModel;

class LogActivity
{
    public static function addToLog($description, $model = NULL)
    {
    	$log = [];
        $log['description'] = $description;
        $log['model'] = $model;
    	$log['url'] = Request::fullUrl();
    	$log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
    	$log['user_id'] = auth()->check() ? auth()->user()->id : null;
        LogActivityModel::create($log);
    }

    public static function logActivityLists()
    {
    	return LogActivityModel::latest()->get();
    }

}