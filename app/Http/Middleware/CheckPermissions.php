<?php

namespace Finzo\Http\Middleware;
use Finzo\Permission;

use Closure;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       //  if($this->hasPermissionForRoute($request)) {
       //      return $next($request);
       //  } 
       // abort(403, 'Access denied');
         return $next($request);
    }

    public function hasPermissionForRoute($routeName)
    {                
        // Check if permission is valid
        if(auth()->user()->hasRole('super_admin')) {
            return true;
        } else if($routeName->route()->getName() != ""
            && (Permission::checkValidPermission($routeName->route()->getName()))) {
            // Check if user has permission
            if(auth()->user()->hasPermissionTo($routeName->route()->getName())) {
                return true;
            }             
        }
        return false;
    }
}
