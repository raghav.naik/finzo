<?php

namespace Finzo\Http\Controllers;

use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function services() 
    {
	    $url ="includes.servicesNav";
	    return view('services')->with('url',$url);
	}
}
