<?php

namespace Finzo\Http\Controllers\StaticPages;

use Illuminate\Http\Request;

class SolutionsController extends Controller
{
    public function solutions() 
    {
	    $url ="includes.solutionsNav";
	    $class = "active";
	    return view('digital_wallet',compact('url','class'));
	}
	public function finzo_pay()
	{
		$url ="includes.solutionsNav";
		$class = "active";
		return view('finzo_pay',compact('url','class'));
	}
	public function digital_banking() 
	{
		$url ="includes.solutionsNav";
		$class = "active";
		return view('digital_banking',compact('url','class'));
	}
}
