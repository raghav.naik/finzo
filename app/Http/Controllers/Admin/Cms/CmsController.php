<?php

namespace Finzo\Http\Controllers\Admin\Cms;

use Illuminate\Http\Request;
use Finzo\Http\Controllers\Controller;
use Finzo\Cms as Cms;

class CmsController extends Controller
{
    public function __construct()
    {
        $this->StaticPage = new Cms();
    }

    public function index($urlSlug)
    {       

        if(!isset($urlSlug)) {
            return response()->json([
                'message' => "Missing Url slug"
            ],400);
        }

        $data       = $this->StaticPage->listStaticPages($urlSlug);
        if(isset($data[0])) {
            // Url Requested
            $requester                  = array();
            $requester["title"]         = $data[0]->title;
            $requester["name"]          = $data[0]->name;
            $requester["description"]   = $data[0]->description;
            $response["requester"]      = $requester;
            // Generate parent pages
            if($data[0]->parent_id == 0) {
                $parentData             = $this->StaticPage->listStaticPages(null, $data[0]->parent_id);
                $parent                 = array();
                $parent["title"]        = $parentData[0]->title;
                $parent["name"]         = $parentData[0]->name;
                $parent["description"]  = $parentData[0]->description;
                $parent["files_id"]     = $parentData[0]->file_id;
                $parent["files_path"]     = $parentData[0]->file_path;
                $response["parent"]     = $parent;
                // Generate child pages 
                $childCount             = $this->StaticPage->listStaticPages(null, null, $data[0]->id, 1);
                $childCount             = $childCount[0]->count;
                if($childCount > 0) {
                    $childData          = $this->StaticPage->listStaticPages(null, null, $data[0]->id);
                    $response["child"]  = $childData;
                }
            }
            return response()->json([
                            'message'   => "Data found",
                            'data'      => $response
            ],200);
        }
        
        return response()->json([
                'message' => "Data not found"
        ],500);
    }
}
