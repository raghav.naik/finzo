<?php

namespace Finzo\Http\Controllers\Admin\Cms;

use Illuminate\Http\Request;
use Finzo\Pages;
use Finzo\Sections;
use Finzo\Files;
use DB;
use Finzo\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $pages = Pages::paginate(10);
       return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages_details = Pages::PagesParent();
        return view('admin.pages.create', compact('pages_details'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      switch ($request->input('action')) 
      {
        case 'save':
          $request->validate([
          'title' => 'nullable',
          'name'=>'required',
          'description'=> 'nullable',          
          'parent_id' => 'nullable'
        ]);

        $id = DB::table('cms_pages')->insertGetId([
          'title' => $request->get('title'),
          'name'=> $request->get('name'),
          'description'=> $request->get('description'),
          'url_slug'=> $request->get('url_slug'),
          'parent_id'=> $request->get('parent_id'),
        ]);

        //Creating Files
        $file_name = $request->get('file_name');
        $file_url = pathinfo($file_name, PHP_URL_PATH).'/'.pathinfo($file_name , PATHINFO_BASENAME);
        $file_path = parse_url($file_url);
        $file_type = pathinfo($file_name , PATHINFO_EXTENSION);
        $file_basename = pathinfo($file_name , PATHINFO_BASENAME);

        $file = new Files([
          'file_name' => $file_basename,
          'file_path' => $file_path['path'],
          'type' => $file_type,
          'asset_id' => $id
        ]);

        $file->save();
        return redirect('admin/pages')->with('success', 'Pages has been added');
        break;

        case 'publish':
        $request->validate([
          'title' => 'nullable',
          'name'=>'required',
          'description'=> 'nullable',          
          'parent_id' => 'nullable'
        ]);
        $id = DB::table('cms_pages')->insertGetId([
          'title' => $request->get('title'),
          'name'=> $request->get('name'),
          'description'=> $request->get('description'),
          'url_slug'=> $request->get('url_slug'),
          'parent_id'=> $request->get('parent_id'),
          'publish'=>1
        ]);
        
        //Creating Files
        $file_name = $request->get('file_name');
        $file_url = pathinfo($file_name, PHP_URL_PATH).'/'.pathinfo($file_name , PATHINFO_BASENAME);
        $file_path = parse_url($file_url);
        $file_type = pathinfo($file_name , PATHINFO_EXTENSION);
        $file_basename = pathinfo($file_name , PATHINFO_BASENAME);

        $file = new Files([
          'file_name' => $file_basename,
          'file_path' => $file_path['path'],
          'type' => $file_type,
          'asset_id' => $id
        ]);

        $file->save();
       //Updated Logs 
       \LogActivity::addToLog('Created', 'CMS Page');        
        return redirect('admin/pages')->with('success', 'Pages has been added');
        break;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pages $page, Sections $section)
    {

        return view('admin.pages.show', compact('section', 'id', 'page_title', 'url_slug', 'templates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pages $page)
    {
        $page_id = $page->id;
        $pages_details = Pages::PagesDetails($page_id);
        $page_parent = Pages::PagesParent();
        $selectedParents = Pages::PagesDetails($page_id);
        $selectedParent = $selectedParents[0]->parent_id;
        return view('admin.pages.edit',compact('page', 'pages_details', 'selectedParent', 'page_parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      switch ($request->input('action')) 
      {
        case 'save':
        $request->validate([
        'title'=>'nullable',
        'name'=>'required',
        'description'=> 'nullable',
        'url_slug'=> 'nullable',         
        'parent_id' => 'nullable',
        'title'=>'nullable',
        ]);

        $page = Pages::find($id);
        $page->title = $request->get('title');
        $page->name = $request->get('name');
        $page->description = $request->get('description');
        $page->url_slug = $request->get('url_slug');
        $page->parent_id = $request->get('parent_id');
        $page->publish = 0;

        //Updating Files
        $file_details = Pages::FileDetails($id);
        $file_name = $request->get('file_name');
        $file_url = pathinfo($file_name, PHP_URL_PATH).'/'.pathinfo($file_name , PATHINFO_BASENAME);
        $file_path = parse_url($file_url);
        $type = pathinfo($file_name , PATHINFO_EXTENSION);
        $file_basename = pathinfo($file_name , PATHINFO_BASENAME);

        $file = Files::find($file_details[0]->id);
        $file->file_name = $file_basename;
        $file->file_path = $file_path['path'];
        $file->type = $type;
        $file->asset_id = $id;
        
        $file->update();
        $page->update($request->all()); 
        return redirect('admin/pages')->with('success', 'Pages has been updated');
        break;

        case 'publish':
        $request->validate([
          'title'=>'nullable',
          'name'=>'required',
          'description'=> 'nullable',
          'url_slug'=> 'nullable',          
          'title'=>'nullable',
          'name'=>'nullable',
          'description'=> 'nullable',          
          'parent_id' => 'nullable'
        ]);

        $page = Pages::find($id);
        $page->title = $request->get('title');
        $page->name = $request->get('name');
        $page->description = $request->get('description');
        $page->url_slug = $request->get('url_slug');
        $page->parent_id = $request->get('parent_id');
        $page->publish = 1;

        //Updating Files
        $file_details = Pages::FileDetails($id);
        $file_name = $request->get('file_name');
        $file_url = pathinfo($file_name, PHP_URL_PATH).'/'.pathinfo($file_name , PATHINFO_BASENAME);
        $file_path = parse_url($file_url);
        $type = pathinfo($file_name , PATHINFO_EXTENSION);
        $file_basename = pathinfo($file_name , PATHINFO_BASENAME);

        $file = Files::find($file_details[0]->id);
        $file->file_name = $file_basename;
        $file->file_path = $file_path['path'];
        $file->type = $type;
        $file->asset_id = $id;
        
        $file->update();
        $page->update($request->all());
       //Updated Logs 
       \LogActivity::addToLog('Updated', 'CMS Page');        
        return redirect('admin/pages')->with('success', 'Pages has been updated');
        break;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Pages::find($id);
        $page->delete();
        //Updated Logs 
        \LogActivity::addToLog('Deleted', 'CMS Page');         
        return redirect('admin/pages')->with('success', 'Pages has been deleted Successfully');
    }
}