<?php

namespace Finzo\Http\Controllers\Admin\Offers;

use Illuminate\Http\Request;
use Finzo\Offers;
use Finzo\Files;
use DB;
use Finzo\Http\Controllers\Controller;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $offers = Offers::paginate(2);
      return view('admin.offers.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //Getting File Id using File name from files table
      $file_value = $request->get('file_id');
      $file =DB::table('files')->where('file_name', $file_value)->select('id')->first();
      $file_id = json_decode( json_encode($file), true);

      $request->validate([
        'title'=>'required',
        'description'=> 'required',
        'file_id' => 'required',
        'priority' => 'nullable',
        'expiry_date' => 'required',
      ]);
      $offer = new Offers([
        'title' => $request->get('title'),
        'description'=> $request->get('description'),
        'file_id'=> $file_id['id'],
        'priority'=> 1,
        'expiry_date'=> $request->get('expiry_date')
      ]);
      $offer->save();
      return redirect('admin/offers')->with('success', 'Offers has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Offers $offer)
    {   
        return view('admin.offers.edit',compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Files $file)
    {
        //Getting File Id using File name from files table
        $file_value = $request->get('file_id');
        $file =DB::table('files')->where('file_name', $file_value)->select('id')->first();
        $file_id = json_decode( json_encode($file), true);

        $request->validate([
        'title'=>'required',
        'description'=> 'required',
        'file_id' => 'required',
        'priority' => 'nullable',
        'expiry_date' => 'required'
      ]);


      $offer = Offers::find($id);
      $offer->title = $request->get('title');
      $offer->description = $request->get('description');
      $offer->file_id = $file_id['id'];
      $offer->priority = $request->get('priority');
      $offer->expiry_date = $request->get('expiry_date');
      $offer->save();

      return redirect('admin/offers')->with('success', 'Offers has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offers::find($id);
        $offer->delete();
        return redirect('admin/offers')->with('success', 'Offers has been deleted Successfully');
    }
}
