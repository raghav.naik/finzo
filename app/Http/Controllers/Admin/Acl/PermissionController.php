<?php

namespace Finzo\Http\Controllers\Admin\Acl;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Finzo\Http\Controllers\Controller;
use Finzo\RoleHasPermission;
class PermissionController extends Controller
{
    public $newRoleId;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::paginate(20); //Get all permissions
        return view('admin/permission/list')->with('permissions', $permissions);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/permission/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permission=$request->name;
        $attribute = $request->validate([
            'title' => 'required',
            'name' => 'required|unique:permissions,name'
        ]);
        Permission::create($attribute);    
        $role = Role::findById(1);
        $role->givePermissionTo($permission);
        //Adding Logs 
        \LogActivity::addToLog('Created', 'Permission');
        return redirect('admin/permissions')
                ->with('alert-success', 'Permission Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permission.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'title'=>'required',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();
        //Updated Logs 
        \LogActivity::addToLog('Updated', 'Permission');        
        return redirect('admin/permissions')
                ->with('alert-success', 'Permission'. $permission->name.' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        //Updated Logs 
        \LogActivity::addToLog('Deleted', 'Permission');
        return redirect('admin/permissions') 
                ->with('alert-success', 'Permission deleted!');

    }

    /**
     * loading the view with and without request parameter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assignPermission(Request $request)
    {  
        $roles = Role::all();
        $permissions = Permission::all();
        $role = Role::findById(1);
        if(isset($permissions)) {
            foreach ($permissions as $key => $value) {     
                if( $role->hasPermissionTo($value)) {
                        $permissions[$key]->checked=true;    
                    }
                }
            }
        $oldPermissions =$permissions;
        if($request->input('role')) {
            $roleId =$request->input('role');
            $permissions = Permission::all();
            $role = Role::findById($roleId);
            if(isset($permissions)) {
                foreach ($permissions as $key => $value) {
                    if( $role->hasPermissionTo($value)) {
                        $permissions[$key]->checked=true;
                        }
                    } 
                }    
            $oldPermissions =$permissions;
            $newRole =$role->id;
             foreach ($roles as $key => $value) {
                if( ($value->id) == $newRole) {
                    $roles[$key]->selected=true;    
                }
            }            
        } 
        return view('admin/permission/assignPermission',compact('oldPermissions','roles','newRole'));
    }
    /**
     * deleting the existing permission (if any) and storing new permission.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePermissionRole(Request $request)
    {  
        $roleId = $request->input('role');    
        $rolePermission = new RoleHasPermission();
        $rolePermission->destroyPermission($roleId); 
        $permissions= $request->input('permission');
        if(isset($permissions)) {       
            foreach ($permissions as $permission) {         
                $role = Role::findById($roleId);
                $role->givePermissionTo($permission);      
            }
        }
       return redirect('admin/permission/assign-permission');     
    }   
}