<?php

namespace Finzo\Http\Controllers\Admin\Acl;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Finzo\Http\Controllers\Controller;


use Illuminate\Http\Request;

class RolesController extends Controller
{
    protected $pageHeading;
    public function __construct()
    {
        $this->pageHeading = "Roles";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageHeading = $this->pageHeading;
        $roles = Role::paginate(5);
        return view('admin.roles.list', compact('roles','pageHeading'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageHeading = "Roles";
        return view('admin.roles.create', compact('pageHeading'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute = $request->validate([
            'title' => 'required',
            'name' => 'required',
            'status' => 'required'
        ]);
        Role::create($attribute);
       //Updated Logs 
       \LogActivity::addToLog('Created', 'Roles');
        return redirect('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Finzo\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function show(Roles $roles)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Finzo\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::findOrFail($id);
        $pageHeading = "Roles";
        return view('admin.roles.edit', compact('roles', 'pageHeading'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Finzo\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $newAttribute = $request->validate([
            'title' => 'required',
            'status' => 'required'
        ]);
        Role::where('id', $id)->update($newAttribute);
       //Updated Logs 
       \LogActivity::addToLog('Updated', 'Roles');
        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Finzo\Roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
