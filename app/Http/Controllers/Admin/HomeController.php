<?php

namespace Finzo\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Finzo\Http\Controllers\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        //\LogActivity::addToLog('My Testing Add To Log.');
//         activity()
//    ->log('edited');
            //Role::create(['name' => 'superadmin2','title' => 'superadmin1']);
        //Permission::create(['name' => 'create usersd2','title' => 'create usersd2']);
        //auth()->user()->givePermissionTo('create users');
        //auth()->user()->assignRole('superadmin');
        //$permission = Permission::create(['name' => 'create users']);
        //$role = Role::findById(1);
        //$role->givePermissionTo('create users');
        //return auth()->user()->getAllPermissions();
        return view('admin.home');
    }
}
