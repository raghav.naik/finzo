<?php

namespace Finzo\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use Finzo\User;
use Input;
use Finzo\ModelHasPermission;
use Finzo\ModelHasRoles;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Crypt;
use Finzo\Http\Controllers\Controller;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::paginate(5);
        //dd($users->getAllPermissions());
        return view('admin/users/list',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('admin/users/create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $user = new User();
        $userData = $this->validate($request, [
            'username'=>'required|unique:users,username',
            'first_name'=>'required',
            'email'=> 'required|email|unique:users,email,$this->id,id',
            'password' => 'required|required_with:password_confirmation|same:password_confirmation|min:8|regex:/^(?=.*[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'password_confirmation' => 'required|min:8',
            'phone'=>'required||unique:users|min:10|numeric',
            'gender'=>'required',
            'status'=>'required',
            ],[ 'password.regex' => 'Password must contain 1 number,1 special character,1 lower case'
           ]);
        $userData['password'] =Crypt::encryptString($request->password);
        $userData['last_name'] =$request->input('last_name');
        $user = User::create($userData);
        $roles = $request['roles'];
        foreach ($roles as $role)
        {
            $role_r = Role::where('id', '=', $role)->firstOrFail();
            $user->assignRole($role_r);
        }
        //Updated Logs 
        \LogActivity::addToLog('Created', 'User');         
       return redirect('admin/users')->with('alert-success', 'User details are saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        $pageHeading = "Users";
        return view('admin.users.edit', compact('user', 'roles','pageHeading'));
    }

     /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        $user = User::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields
        $this->validate($request, [
            'username'=>'required',
            'first_name'=>'required',
            'email'=> 'required|email',
            'phone'=>'required|min:10|numeric',
            'gender'=>'required',
            'status'=>'required',
            ]);
        $input = $request->only(['username', 'email', 'first_name','last_name','phone','gender','status']); //Retreive the name, email and password fields
        $roles = $request['roles']; //Retreive all roles
        $user->fill($input)->save();
        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
        }
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        //Updated Logs 
        \LogActivity::addToLog('Updated', 'User');           
        return redirect('admin/users')
            ->with('alert-success',
             'User successfully edited.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id) {
    //Find a user with a given id and delete
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('admin/users')->with('alert-success', 'User successfully deleted');
    }
      /**
    * Load the view with permission of user and permission of role.
    *
    * @param  int  $id
    * @return string
    */

    public function assignPermission($id){
        $user =  User::findOrFail($id);
        $userId = $user->id;
        $role = new ModelHasRoles;
        $userRoleId = $role->userRole($userId); 
        $roleId = $userRoleId[0]->role_id;
        $userRole = Role::findById($roleId);     
        $permissions = Permission::all();
        $role = Role::all();
        if(isset($permissions)) {
            foreach ($permissions as $key => $value) {     
                if( $userRole->hasPermissionTo($value))
                {
                    $permissions[$key]->disabled=true; 
                }
                if($user->hasPermissionTo($value))
                {
                    $permissions[$key]->checked=true; 
                }
            }
        }
        $oldPermissions = $permissions;
       return view('admin/users/assignPermission',compact('oldPermissions','userId','rolePermissions'));
    }
      /**
    * storing the user permission.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return string
    */

    public function storePermission(Request $request) {
        $userId = $request->input('user'); 
        $rolePermission = new ModelHasPermission;
        $rolePermission->destroyPermission($userId); 
        $permissions= $request->input('permission');
        if(isset($permissions))
        {       
            foreach ($permissions as $permission) 
            {         
                $user = User::findOrFail($userId);
                $user->givePermissionTo($permission);      
            }
        }
       return redirect('admin/permission/user-permission/'.$userId);
    }
}
