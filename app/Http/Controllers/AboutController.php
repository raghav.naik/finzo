<?php

namespace Finzo\Http\Controllers\StaticPages;

use Illuminate\Http\Request;

class AboutController extends Controller
{
   public function about()
   {
	   $url ="includes.aboutNav";
      $class = "active";
		return view('about',compact('url','class'));
	}

   public function about_management()
   {
	   $url ="includes.aboutNav";
      $class = "active";
		return view('about_management',compact('url','class'));
   }
   
   public function about_directors() 
   {
   	$url ="includes.aboutNav";
      $class = "active";
		return view('about_directors',compact('url','class'));
   }
}
  