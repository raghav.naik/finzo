<?php

namespace Finzo\Http\Controllers\StaticPages;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contact() 
    {
	    $url ="includes.contactNav";
	    return view('contact')->with('url',$url);
	}
}
