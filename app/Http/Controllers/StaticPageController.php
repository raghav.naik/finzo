<?php

namespace Finzo\Http\Controllers;

use Illuminate\Http\Request;
use Finzo\Http\Controllers\Admin\Cms\CmsController;
use function GuzzleHttp\json_decode;

class StaticPageController extends Controller
{
    public function index(Request $request) {
        
        $urlSlug = $request->route()->getName();
        //Fetch the content from API
        $content = new CmsController();
        $response = $content->index($urlSlug);        
        if($response->status() != 200) {
            abort(404, "Page you requested not found");
        }
        $pageData = json_decode($response->content());
        switch ($urlSlug) {
            case 'services':
                    return view('templates/services')->with(['pageData'=>$pageData->data]);;
                break;
            
            default:
                # code...
                break;
        }
    }
}
