<?php

namespace Finzo\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Finzo\Http\Requests;
use Finzo\ContactUS;

class ContactUSController extends Controller
{
   public function create()
    {
        //
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email|max: 255',
        'phone' => 'required|integer',
        'subject' => 'nullable' ,
        'message' => 'nullable'
        ]);
 	
       ContactUS::create($request->all());
       return back()->with('success', 'Thanks for contacting us!');
    }
}
