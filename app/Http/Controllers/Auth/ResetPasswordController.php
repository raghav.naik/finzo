<?php

namespace Finzo\Http\Controllers\Auth;

use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Finzo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Str;
use Finzo\PasswordHistory;
use Finzo\passwordSecurity;
use Carbon\Carbon;
use Config;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }

    public function changePassword(Request $request) {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        // validating the fields
        $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        $user = Auth::user();
        // Check Password History
        $this->checkPasswordHistory($request, $user);

        // Save new password
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        // Entry into history table
        $this->savePasswordHistory($request, $user);

        return redirect()->back()->with("success","Password changed successfully !");

    }

    public function checkPasswordHistory($request, $user) {
        $user = Auth::user();
        $passwordHistories = $user->passwordHistories()->orderBy('id','desc')->take(Config::get('app.password-history-num'))->get();
        foreach($passwordHistories as $passwordHistory) {
            if (Hash::check($request->get('new-password'), $passwordHistory->password)) {
                // The passwords matches
                return redirect()->back()->with("error","Your new password can not be same as any of your recent passwords. Please choose a new password.");
            }
        }
    }

    public function savePasswordHistory($request, $user) {
        PasswordHistory::create([
            'user_id' => $user->id,
            'password' => bcrypt($request->get('new-password'))
        ]);
        $user->passwordSecurity->password_updated_at = Carbon::now();
        $user->passwordSecurity->save();
    }
}