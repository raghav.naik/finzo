<?php

namespace Finzo\Http\Controllers\Auth;

use Finzo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Finzo\passwordSecurity;
use Illuminate\Http\Request;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = $this->field($request);
        return [
            $field => $request->get($this->username()),
            'password' => $request->get('password'),
        ];
    }

    /**
     * Determine if the request field is email or username.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function field(Request $request)
    {
        $email = $this->username();
        return filter_var($request->get($email), FILTER_VALIDATE_EMAIL) ? $email : 'username';
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $field = $this->field($request);
        $messages = ["{$this->username()}.exists" => 'The account you are trying to login is not registered or it has been disabled.'];
        $this->validate($request, [
            $this->username() => "required|exists:users,{$field}",
            'password' => 'required',
        ], $messages);
    }


    public function authenticated(Request $request, $user)
    {
        $password_updated_at = NULL;
        $password_expiry_days = NULL;
        $request->session()->forget('password_expired_id');
        if($user->passwordSecurity) {
            $password_updated_at = $user->passwordSecurity->password_updated_at;
            $password_expiry_days = $user->passwordSecurity->password_expiry_days;
        }
        if($password_updated_at && $password_expiry_days) {
            $password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);
            if($password_expiry_at->lessThan(Carbon::now())) {
                $request->session()->put('password_expired_id',$user->id);
                auth()->logout();
                return redirect('/passwordExpiration')->with('message', "Your Password is expired, You need to change your password.");
            }
        }
        return redirect()->intended($this->redirectPath());
    }
}
