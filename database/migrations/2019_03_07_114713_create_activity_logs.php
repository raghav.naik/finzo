<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url',255);
            $table->string('method',50);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
              ->onDelete('cascade');
            $table->text('description');
            $table->string('ip',255);
            $table->string('agent')->nullable();
            $table->string('model', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_log');
    }
}
