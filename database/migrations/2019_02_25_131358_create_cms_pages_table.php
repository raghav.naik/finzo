<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cms_pages', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title');
        $table->string('name');
        $table->text('description');
        $table->string('url_slug');
        $table->unsignedInteger('template_id');
        $table->foreign('template_id')->references('id')->on('page_template');
        $table->unsignedInteger('parent_id');
        $table->boolean('publish')->default(false);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pages');
    }
}
