<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [];
        // MANAGE USERS
        $permission[] = Permission::create(['title' => 'Create User', 'name' => 'users.create']);
        $permission[] = Permission::create(['title' => 'Update User', 'name' => 'users.edit']);
        $permission[] = Permission::create(['title' => 'Delete User', 'name' => 'users.destroy']);
        $permission[] = Permission::create(['title' => 'Manage User', 'name' => 'users.index']);

        //MANAGE Promotions
        $permission[] = Permission::create(['title' => 'Create Promotions', 'name' => 'promotions.create']);
        $permission[] = Permission::create(['title' => 'Update Promotions', 'name' => 'promotions.edit']);
        $permission[] = Permission::create(['title' => 'Delete Promotions', 'name' => 'promotions.destroy']);
        $permission[] = Permission::create(['title' => 'Manage Promotions', 'name' => 'promotions.index']);

        //MANAGE Offers
        $permission[] = Permission::create(['title' => 'Create Offers', 'name' => 'offers.create']);
        $permission[] = Permission::create(['title' => 'Update Offers', 'name' => 'offers.edit']);
        $permission[] = Permission::create(['title' => 'Delete Offers', 'name' => 'offers.destroy']);
        $permission[] = Permission::create(['title' => 'Manage Offers', 'name' => 'offers.index']);

        //MANAGE ROLES 
        $permission[] = Permission::create(['title' => 'Create Roles', 'name' => 'roles.create']);
        $permission[] = Permission::create(['title' => 'Update Roles', 'name' => 'roles.edit']);
        $permission[] = Permission::create(['title' => 'Manage Roles', 'name' => 'roles.index']);

        //MANAGE PERMISSIONS
        $permission[] = Permission::create(['title' => 'Create Permissions', 'name' => 'permissions.create']);
        $permission[] = Permission::create(['title' => 'Update Permissions', 'name' => 'permissions.edit']);
        $permission[] = Permission::create(['title' => 'Delete Permissions', 'name' => 'permissions.destroy']);
        $permission[] = Permission::create(['title' => 'Manage Permissions', 'name' => 'permissions.index']);

        //MANAGE ROLES & PERMISSIONS
        $permission[] = Permission::create(['title' => 'Update Roles & Permission', 'name' => 'assign.roles.permission']);
        $permission[] = Permission::create(['title' => 'Manage Roles & Permission', 'name' => 'view.roles.permission']);

        //MANAGE PAGES
        $permission[] = Permission::create(['title' => 'Create Pages', 'name' => 'pages.create']);
        $permission[] = Permission::create(['title' => 'Update Pages', 'name' => 'pages.edit']);
        $permission[] = Permission::create(['title' => 'Delete Pages', 'name' => 'pages.destroy']);
        $permission[] = Permission::create(['title' => 'Manage Pages', 'name' => 'pages.index']);

        //VIEW DASHBOARD
        $permission[] = Permission::create(['title' => 'View Dashboard', 'name' => 'dashboard']);        
        
        $role = Role::findById(1);
        foreach($permission as $row) {
            $role->givePermissionTo($row);
        }

    }
}
