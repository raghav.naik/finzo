<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Finzo\User;
class RolesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // CREATING ROLES
        $roles = [];
        $roles[] = Role::create(['title' => 'Super Admin', 'name' => 'super_admin']);
        $roles[] = Role::create(['title' => 'Admin', 'name' => 'admin']);
        $roles[] = Role::create(['title' => 'User', 'name' => 'user']);

        $user = User::findOrFail(1);
        $user->assignRole($roles);

    }
}
