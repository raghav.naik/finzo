<?php

use Illuminate\Database\Seeder;
use Finzo\User;
class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new User();
        $user->username = 'admin';
        $user->first_name = 'administrator';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('123456');
        $user->save();

        // Creating Random Users
        for ($i=0; $i < 5; $i++) {
	    	User::create([
                'username' => str_random(8),
                'first_name' => str_random(8),
	            'email' => str_random(12).'@mail.com',
	            'password' => bcrypt('123456')
	        ]);
        }
    }
}
